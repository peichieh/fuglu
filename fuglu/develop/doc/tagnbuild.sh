#!/bin/bash

# run in fuglu repository root directory as
# bash fuglu/develop/doc/tagnbuild.sh

cd fuglu/src
FUGLUVERSION=$(python3 -c 'from fuglu import __version__; print(__version__)')
echo "tagging version $FUGLUVERSION"
cd ..

git tag -a $FUGLUVERSION -m "tag $FUGLUVERSION"
git push --tags origin master
git push --tags upstream master

python3 setup.py sdist > /dev/null
twine upload dist/fuglu-$FUGLUVERSION.tar.gz
cd ..