#!/usr/bin/env python3
# deploy:
#./make_sphinx_plugin_doc.py > fuglu/documentation/source/includedplugins-autogen.txt
# cd fuglu/documentation
# make html
# firefox index.html
# check for errors, update docstring in plugin if necessary!
# git commit -a -m 'updated doc'
# git push

import inspect
import sys
import os

this_file = inspect.currentframe().f_code.co_filename
workdir = os.path.dirname(os.path.abspath(this_file))
os.chdir(workdir)
sys.path.insert(0, '../../src')

import fuglu
from fuglu.plugins import *
from configparser import RawConfigParser


if __name__ == '__main__':
    config = RawConfigParser()

    pluginlist = [
        sa.SAPlugin(config),
        clamav.ClamavPlugin(config),
        antivirus.SSSPPlugin(config),
        antivirus.FprotPlugin(config),
        antivirus.ICAPPlugin(config),
        antivirus.DrWebPlugin(config),
        antivirus.CyrenPlugin(config),
        antivirus.FuzzyHashCheck(config),
        attachment.FiletypePlugin(config),
        attachment.FileHashCheck(config),
        attachment.EncryptedArchives(config),
        archive.ArchivePlugin(config),
        bacn.BACN(config),
        decision.ActionOverridePlugin(config),
        decision.ConditionalRcptAppend(config),
        decision.FilterDecision(config),
        decision.DeliverDecision(config),
        decision.KillerPlugin(config),
        decision.RcptRewrite(config),
        delay.DelayPlugin(config),
        dnsdata.DNSData(config),
        domainauth.ARCSignPlugin(config),
        domainauth.ARCVerifyPlugin(config),
        domainauth.DKIMSignPlugin(config),
        domainauth.DKIMVerifyPlugin(config),
        domainauth.DMARCPlugin(config),
        domainauth.SPFPlugin(config),
        domainauth.SPFOut(config),
        domainauth.SenderRewriteScheme(config),
        domainauth.DomainAuthPlugin(config),
        fuzor.FuzorCheck(config),
        fuzor.FuzorReport(config),
        fuzor.FuzorReportAppender(config),
        geoip.GeoIPLookup(config),
        geoip.ASNLookup(config),
        knownsubject.KnownSubject(config),
        knownsubject.KnownSubjectAppender(config),
        mailcopy.AutoReport(config),
        mailcopy.IMAPCopyPlugin(config),
        mailcopy.MailFeed(config),
        messagesize.MessageSize(config),
        rspamd.RSpamdPlugin(config),
        script.ScriptFilter(config),
        uriextract.URIExtract(config),
        uriextract.EmailExtract(config),
        uriextract.QRExtract(config),
        uriextract.DomainAction(config),
        uriextract.EmailAction(config),
        vacation.VacationPlugin(config),
        
        # milter / postomaat imports
        call_ahead.AddressCheck(config),
        originpolicy.OriginPolicy(config),
        outpolicy.SenderDomainRules(config),
        outpolicy.NoBounce(config),
        outpolicy.MilterData2Header(config),
        outpolicy.PFQDRateLimit(config),
        outpolicy.ToCCLimit(config),
        restrictions.AccessRestrictions(config),
        tlspolicy.EnforceTLS(config),
        tlspolicy.TLSSender(config),

        # prependers
        p_blwl.BlockWelcomeList(config),
        p_skipper.PluginSkipper(config),
        p_fraction.PluginFraction(config),
        p_debug.MessageDebugger(config),

        # appenders
        a_statsd.PluginTime(config),
        a_statsd.MessageStatus(config),
        a_statsd.MessageStatusPerRecipient(config),
        a_logging.ElasticLogger(config),
    ]

    headerchar = '.'

    subheaderchar = '-'

    for plugin in pluginlist:
        plug_docstring = plugin.__doc__
        if plug_docstring is None:
            plug_docstring = ''
        plug_class = plugin.__class__.__name__
        plug_module = plugin.__module__
        plug_fqdn = "%s.%s" % (plug_module, plug_class)
        plug_humanreadable = str(plugin)

        sphinxdoc = ""

        # write the header
        sphinxdoc += "%s\n" % plug_humanreadable
        sphinxdoc += "".join([headerchar for x in plug_humanreadable]) + "\n"
        sphinxdoc += "\n"

        # info
        sphinxdoc += "Plugin: %s\n" % plug_fqdn
        sphinxdoc += "\n"

        # write the docstring
        sphinxdoc += plug_docstring + "\n\n"

        # config
        configsubtitle = "Configuration"
        sphinxdoc += "%s\n" % configsubtitle
        sphinxdoc += "".join([subheaderchar for x in configsubtitle]
                             ) + "\n\n::\n\n"
        tab = "    "
        sphinxdoc += tab + "[%s]\n" % plug_class
        for optionname, infodic in plugin.requiredvars.items():
            defaultval = ''

            if 'default' in infodic:
                defaultval = infodic['default']

            if 'description' in infodic:
                description = infodic['description']
                description = description.replace('\n', '\n' + tab + '#')
                sphinxdoc += tab + "#%s\n" % description
            try:
                sphinxdoc += tab + optionname + "=" + defaultval + "\n\n"
            except TypeError as e:
                print('failed to concat value of var %s from plugin %s' % (optionname, plug_class))
                raise

        print("")
        print(sphinxdoc)
