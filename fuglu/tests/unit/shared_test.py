# -*- coding: UTF-8 -*-
import operator
import unittest
import string
import email
import os
import sys
import datetime
from email.header import Header
from configparser import ConfigParser
from unittest.mock import patch

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)

from fuglu.stringencode import force_uString, force_bString
from .unittestsetup import TESTDATADIR
from fuglu.shared import (
    Suspect,
    SuspectFilter,
    string_to_actioncode,
    actioncode_to_string,
    apply_template,
    REJECT,
    DUNNO,
    FileList,
    ScannerPlugin,
    FuConfigParser,
    BasicPlugin,
    fold_header,
    redact_uri
)
from fuglu.addrcheck import Addrcheck
from fuglu.connectors.smtpconnector import buildmsgsource

# expected return types
#
# the explicit types for Python 2 and 3 are defined
# in the test for stringencode, see "stringencode_test"
stringtype = type(force_uString("test"))
bytestype = type(force_bString("test"))


class SuspectTestCase(unittest.TestCase):

    """Test Suspect functions"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_id(self):
        """Check the length and uniqueness of the generated id"""
        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        known = []
        for i in range(10000):
            suspect_id = Suspect.generate_id()
            self.assertTrue(
                suspect_id not in known, 'duplicate id %s generated' % suspect_id)
            known.append(suspect_id)
            self.assertEqual(len(suspect_id), 32)
            for c in suspect_id:
                self.assertTrue(c in string.hexdigits)

    def test_tmpfile_given(self):
        """Suspect creation with given filename"""
        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        self.assertEqual(s.tempfile, '/dev/null')
        self.assertEqual(s.tempfilename(), '/dev/null')

    def test_tmpfile_noname_nobuffer(self):
        """No tmpfilename and no buffer should raise an error"""
        with self.assertRaises(AssertionError):
            s = Suspect('sender@example.com', 'recipient@example.com', None)

    def test_tmpfile_created(self):
        """Suspect creation with buffer"""
        s = Suspect('sender@example.com', 'recipient@example.com', None, inbuffer=b"test")
        self.assertEqual(s.tempfilename(), '(buffer-only)')

        # asking for the filename will push the buffer to a temporary file
        tmpfilename = s.tempfile
        print(f"filename: {tmpfilename}")
        self.assertNotEqual(s.tempfilename(), '(buffer-only)')

    def test_from_to_parsing(self):
        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')

        self.assertEqual("sender@example.com", s.from_address)
        self.assertEqual("sender", s.from_localpart)
        self.assertEqual("example.com", s.from_domain)

        self.assertEqual("recipient@example.com", s.to_address)
        self.assertEqual("recipient", s.to_localpart)
        self.assertEqual("example.com", s.to_domain)

        for sender in (None, ''):
            s = Suspect(sender, 'recipient@example.com', '/dev/null')
            self.assertEqual("", s.from_address)
            self.assertEqual("", s.from_localpart)
            self.assertEqual("", s.from_domain)

            self.assertEqual("recipient@example.com", s.to_address)
            self.assertEqual("recipient", s.to_localpart)
            self.assertEqual("example.com", s.to_domain)

    def test_from_to_local_addr(self):
        """Make sure local senders / recipients are accepted"""
        s = Suspect('bob@localhost', 'root@localhost', '/dev/null')
        self.assertEqual("bob@localhost", s.from_address)
        self.assertEqual("bob", s.from_localpart)
        self.assertEqual("localhost", s.from_domain)

        self.assertEqual("root@localhost", s.to_address)
        self.assertEqual("root", s.to_localpart)
        self.assertEqual("localhost", s.to_domain)


    def test_from_to_illegal(self):
        """Make sure invalid sender/recipient addresses raise a ValueError"""
        self.assertRaises(ValueError, Suspect, "sender@example.com", None, '/dev/null')
        self.assertRaises(ValueError, Suspect, "sender@example.com", "recipient", '/dev/null')
        self.assertRaises(ValueError, Suspect, "sender", "recipient@example.com", '/dev/null')
        self.assertRaises(ValueError, Suspect, "sender@example.net", "recipient@example.com@example.org", '/dev/null')

    def test_special_local_part(self):
        """Make sure Sender/Receiver with quoted local part are received correctly and can contain '@'"""
        Addrcheck().set("Default")
        self.assertRaises(ValueError, Suspect, "sender@example.net", "recipient@example.com@example.org", '/dev/null')

        Addrcheck().set("LazyLocalPart")
        self.assertRaises(ValueError, Suspect, '"bob@remotehost"@localhost', "'root@localhost'@remotehost", '/dev/null')

        s = Suspect('"bob@remotehost"@localhost', '"root@localhost"@remotehost', '/dev/null')
        self.assertEqual('"bob@remotehost"@localhost', s.from_address)
        self.assertEqual('"bob@remotehost"', s.from_localpart)
        self.assertEqual("localhost", s.from_domain)

        self.assertEqual('"root@localhost"@remotehost', s.to_address)
        self.assertEqual('"root@localhost"', s.to_localpart)
        self.assertEqual("remotehost", s.to_domain)


    def test_nocheck(self):
        """No address check"""
        Addrcheck().set("NoCheck")
        Suspect('"bob@remotehost"@localhost', "'root@localhost'@remotehost", '/dev/null')
        Suspect('invalid-address', "", '/dev/null')

    def test_ascii(self):
        from fuglu.addrcheck import AsciiOnly
        ascii_check = AsciiOnly()
        self.assertTrue(ascii_check("abc@efg"))
        self.assertFalse(ascii_check("abc@efg@hij"))
        self.assertTrue(ascii_check('"abc@efg"@hij'))
        self.assertFalse(ascii_check("aböc@efg"))
        self.assertTrue(ascii_check('"abc@efg"@hij'))

    def test_printable_ascii(self):
        """Printable ascii address checker"""
        from fuglu.addrcheck import PrintableAsciiOnly
        ascii_check = PrintableAsciiOnly()
        self.assertTrue(ascii_check('abc@hij'), "simple valid (note: domain doesn't matter)")
        self.assertFalse(ascii_check('abc@efg@hij'), "invalid, '@' is not qouted")
        self.assertFalse(ascii_check("ab\x07c@efg"), "invalid, mail contains non-printable ascii")
        self.assertTrue(ascii_check('"abc@efg"@hij'), "valid, '@' is qouted")

    def test_postmaster(self):
        """Postmaster as recipient without domain is allowed"""
        Addrcheck().set("LazyLocalPart")
        s = Suspect('test@localhost', 'postmaster@remotehost', '/dev/null')
        s = Suspect('test@localhost', 'postmaster', '/dev/null')
        with self.assertRaises(ValueError):
            s = Suspect('test@localhost', 'invalid', '/dev/null')

    def test_return_types(self):
        """Test main routine return types for Python 2/3 consistency"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        headerstring = suspect.get_headers()
        self.assertEqual(type(headerstring),stringtype,"Wrong return type for get_headers")

        source = suspect.get_source()
        self.assertEqual(type(source),bytestype,"Wrong return type for get_source")

        source = suspect.get_original_source()
        self.assertEqual(type(source),bytestype,"Wrong return type for get_original_source")

    def test_set_source(self):
        """Test main set_source for Python 2/3 consistency with different input types"""
        suspect = Suspect( 'sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')

        suspectorig = Suspect('sender@unittests.fuglu.org',
                              'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        test_source_binary = suspectorig.get_source()
        test_source_unicode = force_uString(test_source_binary)

        suspect.set_source(test_source_binary)
        self.assertEqual(type(suspect.get_source()),bytestype,"Wrong return type for get_source after setting binary source")
        self.assertEqual(suspect.get_source(),test_source_binary,"Binary source content has to remain the same")

        suspect.set_source(test_source_unicode)
        self.assertEqual(type(suspect.get_source()),bytestype,"Wrong return type for get_source after setting unicode source")
        self.assertEqual(suspect.get_source(),test_source_binary,"Binary source content has to remain the same as the unicode content sent in")

    def test_add_header(self):
        """Test add_header for Python 2/3 consistency with different input types"""
        suspectorig = Suspect('sender@unittests.fuglu.org',
                              'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        newheader = ("x-new-0","just a test for default string type")
        newheaderb = (b"x-new-1",b"just a test encoded strings")
        newheaderu = (u"x-new-2",u"just a test unicode strings")

        # new dummy suspect with a copy of data from helloworld
        suspect = Suspect( 'sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.setSource(suspectorig.get_original_source())

        suspect.add_header(*newheader,immediate=True)
        suspect.add_header(*newheaderb,immediate=True)
        suspect.add_header(*newheaderu,immediate=True)

        # check headers just set
        msg = suspect.get_message_rep()
        self.assertEqual(force_uString( newheader[1]),msg["x-new-0"])
        self.assertEqual(force_uString(newheaderb[1]),msg["x-new-1"])
        self.assertEqual(force_uString(newheaderu[1]),msg["x-new-2"])

    def test_message_flatten(self):
        """Python-3.x seems to have a problem to flatten some messages"""

        # Until https://bugs.python.org/issue27321 is merged we need
        # a patched version already implementing this fix
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/contentproblem.eml')
        mailobj = suspect.get_message_rep()
        mailstr = mailobj.as_string()

    def test_message_flatten_patchneeded(self):
        """Development: check if fix is still needed!"""

        # see "test_message_flatten" test
        with patch('fuglu.shared.PatchedMessage', wraps=email.message.Message):
            suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/contentproblem.eml')
            mailobj = suspect.get_message_rep()
            if sys.version_info > (3, 9, 1):
                print(f"Patch not needed with version {sys.version_info}")
                _ = mailobj.as_string()
            else:
                with self.assertRaises(KeyError, msg="If there's no KeyError anymore change the import shared.py "
                                                     "-> PatchedMessage is not needed for versions > (%u,%u,%u)"
                                                     % (sys.version_info.major, sys.version_info.minor,
                                                        sys.version_info.micro)):
                    _ = mailobj.as_string()

    def test_message_flatten_patchneeded2(self):
        """Development: check if fix is still needed even using EmailMessage object!"""

        # see "test_message_flatten" test
        with patch('fuglu.shared.PatchedMessage', wraps=email.message.EmailMessage):
            suspect = Suspect('sender@unittests.fuglu.org',
                              'recipient@unittests.fuglu.org', TESTDATADIR + '/contentproblem.eml')
            mailobj = suspect.get_message_rep()

            if sys.version_info > (3, 9, 1):
                print(f"Patch not needed with version {sys.version_info}")
                _ = mailobj.as_string()
            else:
                with self.assertRaises(KeyError, msg="If there's no KeyError anymore change the import shared.py "
                                                     "-> PatchedMessage is not needed for versions > (%u,%u,%u)"
                                                     % (sys.version_info.major, sys.version_info.minor,
                                                        sys.version_info.micro)):
                    _ = mailobj.as_string()

    def test_suspect_decode_msg_header(self):
        """Test static function "decode_msg_header" of suspect"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # check headers just set
        msg = suspect.get_message_rep()

        newheader0 = "test direct string"
        newheader1 = "direct string in header object"
        newheader2 = "unicode chars: ünicödé chàrs"

        msg["x-new-0"] = newheader0
        msg["x-new-1"] = Header(newheader1).encode()
        msg["x-new-2"] = Header(newheader2, charset='utf-8').encode()

        self.assertEqual(newheader0, suspect.decode_msg_header(msg["x-new-0"]))
        self.assertEqual(newheader1, suspect.decode_msg_header(msg["x-new-1"]))
        self.assertEqual(newheader2, suspect.decode_msg_header(msg["x-new-2"]))

        # check if it's possible to apply strip (should be string, so ok)

        self.assertEqual(newheader0.strip(), suspect.decode_msg_header(msg["x-new-0"]).strip())
        self.assertEqual(newheader1.strip(), suspect.decode_msg_header(msg["x-new-1"]).strip())
        self.assertEqual(newheader2.strip(), suspect.decode_msg_header(msg["x-new-2"]).strip())

    def test_suspect_decode_msg_header_strip(self):
        """Test static function "decode_msg_header" of suspect with strip function applied"""

        # originates from a bug report where the return type Header did not
        # have "strip" as a member function

        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # check headers just set
        msg = suspect.get_message_rep()

        newheader0 = "test direct string"
        newheader1 = "direct string in header object"
        newheader2 = "unicode chars: ünicödé chàrs"

        msg["x-new-0"] = "   "+newheader0+" "
        msg["x-new-1"] = Header("   "+newheader1+" ").encode()
        msg["x-new-2"] = Header("   "+newheader2+" ", charset='utf-8').encode()

        self.assertEqual(newheader0, suspect.decode_msg_header(msg["x-new-0"]).strip())
        self.assertEqual(newheader1, suspect.decode_msg_header(msg["x-new-1"]).strip())
        self.assertEqual(newheader2, suspect.decode_msg_header(msg["x-new-2"]).strip())

        # check if it's possible to apply strip (should be string, so ok)

        self.assertEqual(newheader0.strip(), suspect.decode_msg_header(msg["x-new-0"]).strip())
        self.assertEqual(newheader1.strip(), suspect.decode_msg_header(msg["x-new-1"]).strip())
        self.assertEqual(newheader2.strip(), suspect.decode_msg_header(msg["x-new-2"]).strip())

    def test_suspect_decode_msg_header_multi(self):
        """Test static function "decode_msg_header" for message with two encodings plus an unencoded part"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # check headers just set
        msg = suspect.get_message_rep()

        encodedheader = "AAA; AAA: AAAA AAAAAAAAAAA AAAAAAA =?utf-8?Q?AAAAAAAA=C3=BCAAA?= " \
                        "=?utf-8?Q?A_A=C3=BCA?= AA-AAAAAAAAA DDDD"

        expected = "AAA; AAA: AAAA AAAAAAAAAAA AAAAAAA AAAAAAAAüAAAA AüA AA-AAAAAAAAA DDDD"

        msg["x-new-2"] = encodedheader

        self.assertEqual(expected, suspect.decode_msg_header(msg["x-new-2"]))

    def test_suspect_decode_msg_header2(self):
        """Test static function "decode_msg_header" for header with only two characters being encoded"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # check headers just set
        msg = suspect.get_message_rep()

        encodedheader = "aa aaaaaaaa aaaaaaa : aa aaa =?iso-8859-1?q?aa=E9aa_aaaa_aaaaa_=E0?= " \
                        "aaaa aa aaaa aaaaaa aaaaaaaaa aaaaaaa aa aaaa"

        expected = "aa aaaaaaaa aaaaaaa : aa aaa aaéaa aaaa aaaaa à aaaa aa aaaa aaaaaa aaaaaaaaa aaaaaaa aa aaaa"

        msg["x-new-2"] = encodedheader
        out = suspect.decode_msg_header(msg["x-new-2"])

        self.assertEqual(expected, out)

    def test_suspect_decode_msg_header_ignorechar(self):
        """By default, replace unknown characters in decode_msg_headers"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # check headers just set
        msg = suspect.get_message_rep()

        encodedheader = "=?UTF-8?B?Y2hlY2sgdGhpcyBjaGFyOiDiiIA=?="
        expected = "check this char: ∀"
        msg["x-new-1"] = encodedheader
        out = suspect.decode_msg_header(msg["x-new-1"])
        self.assertEqual(expected, out, "The original case with correct encoding")

        encodedheader = "=?UTF-8?B?Y2hlY2sgdGhpcyBjaGFyOiDiiI?="
        expected = "check this char: �"

        msg["x-new-2"] = encodedheader
        out = suspect.decode_msg_header(msg["x-new-2"])
        self.assertEqual(expected, out, "Due to the broken encoding the corrupted char should be replaced")

    def test_multiline_from(self):
        """Test parsing of from header, encoded and split on two lines"""

        file = os.path.join(TESTDATADIR, "from_subject_2lines.eml")
        suspect = Suspect('sender@fuglu.org', 'recipient@fuglu.org', file)
        source = force_uString(suspect.get_source())
        print(source)

        # From mail header, encoded and split on two lines
        #
        # From: "=?UTF-8?B?dGhpcyBpcyBmcm9tOiA=?=
        #  =?UTF-8?B?RlVHTFU=?=" <fuglu_from@evil1.unittests.fuglu.org>

        found_mail_list = suspect.parse_from_type_header()
        self.assertEqual([('this is from: FUGLU', 'fuglu_from@evil1.unittests.fuglu.org')], found_mail_list)

    def test_unicode_from_encoding(self):
        """Test parsing of from header with unicode in display name"""

        file = os.path.join(TESTDATADIR, "unicode_from_to.eml")
        suspect = Suspect('sender@fuglu.org', 'recipient@fuglu.org', file)
        source = force_uString(suspect.get_source())
        print(source)

        found_mail_list = suspect.parse_from_type_header()
        self.assertEqual([('sänder', 'sender@unittests.fuglu.org')], found_mail_list)

    def test_unicode_to_noencoding(self):
        """Test parsing of to header with unicode in display name but no encoding"""

        file = os.path.join(TESTDATADIR, "unicode_from_to.eml")
        suspect = Suspect('sender@fuglu.org', 'recipient@fuglu.org', file)
        source = force_uString(suspect.get_source())
        print(source)

        found_mail_list = suspect.parse_from_type_header(header="To")
        print(found_mail_list)
        # display name will not be correct, but mail address should be correclty detected
        self.assertEqual('recipient@unittests.fuglu.org', found_mail_list[0][1])

    def test_unicode_to_noencoding_comma(self, use_cache=False):
        """Test parsing of to header with unicode in display name but no encoding"""

        file = os.path.join(TESTDATADIR, "unicode_from_to.eml")
        suspect = Suspect('sender@fuglu.org', 'recipient@fuglu.org', file)
        source = force_uString(suspect.get_source())
        print(source)

        found_mail_list = suspect.parse_from_type_header(header="Reply-To")
        print(found_mail_list)
        # display name will not be correct, but mail address should be correctly detected
        self.assertEqual('recipient@unittests.fuglu.org', found_mail_list[0][1])

    def test_from_header_doublebracket(self):
        """Test extraction of a from header with address given like <<address>>"""
        # start with hello world template
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()
        # delete existing From header
        del msg["From"]
        # add new From header
        msg["From"] = "Sender <<sender@fuglu.org>>"
        suspect.set_message_rep(msg)
        sender_list = suspect.parse_from_type_header(header="From")
        self.assertEqual([("Sender", "sender@fuglu.org")], sender_list)

    def test_from_header_bad(self):
        """Don't get confused by < in display name"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()
        # delete existing From header
        del msg["From"]
        # add new From header
        msg["From"] = "Sender <noreply.abcd <sender@fuglu.org>"
        suspect.set_message_rep(msg)
        sender_list = suspect.parse_from_type_header(header="From")
        self.assertEqual([("Sender noreply.abcd", "sender@fuglu.org")], sender_list)

    def test_from_header_combine_and_multiple(self):
        """Pushing bad entries to the display part should also work with 2 addresses"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()
        # delete existing From header
        del msg["To"]
        # add new From header
        msg["To"] = "Palim <palim <sender@fuglu.org>, Sender <sender@fuglu.org"
        suspect.set_message_rep(msg)
        receiver_list = suspect.parse_from_type_header(header="To")
        self.assertEqual([('Palim palim', 'sender@fuglu.org'), ('Sender', 'sender@fuglu.org')], receiver_list)

    def test_from_header_combine_and_multiple_extended(self):
        """Pushing bad entries to the display par """
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()
        # delete existing From header
        del msg["To"]
        # add new From header
        msg["To"] = "Receiver <receiver[at]fuglu.org>, Palim <palim <sender@fuglu.org>, Sender <sender@fuglu.org"
        suspect.set_message_rep(msg)
        receiver_list = suspect.parse_from_type_header(header="To")
        self.assertEqual([('Receiver receiver at fuglu.org Palim palim', 'sender@fuglu.org'), ('Sender', 'sender@fuglu.org')], receiver_list)

    def test_empty_from_header(self):
        """Test empty from header"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()
        # delete existing From header
        del msg["From"]
        # add new From header
        msg["From"] = "Sender <>"
        suspect.set_message_rep(msg)
        sender_list = suspect.parse_from_type_header(header="From")

        # in this case there's not valid address extracted
        self.assertEqual([], sender_list)

    def test_empty_from_header_noval(self):
        """Test empty from header without validation"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()
        # delete existing From header
        del msg["From"]
        # add new From header
        msg["From"] = "Sender <>"
        suspect.set_message_rep(msg)
        sender_list = suspect.parse_from_type_header(header="From")

        # without validation, we can still extract the display name and the empty address
        sender_list = suspect.parse_from_type_header(header="From", validate_mail=False, use_cache=False)
        self.assertEqual([("Sender", "")], sender_list)

    def test_to_header_unicode_noencoding(self):
        """Test parsing of "to"-header containing a special char but no encoding"""

        file = os.path.join(TESTDATADIR, "nonascii_env_rcpt.eml")
        suspect = Suspect('sender@fuglu.org', 'recipient@fuglu.org', file)
        source = force_uString(suspect.get_source())

        found_mail = suspect.parse_from_type_header(header='to')[0][1]
        self.assertEqual('a{IGNORE}aa.aaaaaa@aaaaaa.aa', found_mail[:1]+"{IGNORE}"+found_mail[2:])
        # Note the original to-header looks like:
        #
        # To: aüaa.aaaaaa@aaaaaa.aa
        #
        # which is not allowed. So the character that is extracted for "ü" seems to depend on
        # python version, so in this test it is ignored and replaced by {IGNORE}

    def test_with_comment(self):
        """Test empty from header without validation"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()
        # delete existing From header
        del msg["From"]
        # add new From header
        msg["From"] = '"AAAAAAAAAAA" <sender@fuglu.org> (BBBBBBBBBBB)'
        suspect.set_message_rep(msg)
        sender_list = suspect.parse_from_type_header(header="From")

        # without validation, we can still extract the display name and the empty address
        sender_list = suspect.parse_from_type_header(header="From", validate_mail=False, use_cache=False)
        self.assertEqual([("AAAAAAAAAAA", "sender@fuglu.org")], sender_list)

    def test_from_type_header_encoded(self):
        """Test extraction when whole header is encoded, not only displaypart"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msg = suspect.get_message_rep()

        fromheader_raw = "Foo Bar <foo.bar@domain.invalid>"
        print("From header before encoding: %s" % fromheader_raw)
        fromheader_enc = Header(fromheader_raw, charset='utf-8', header_name="From", continuation_ws=' ').encode()
        print("From header encoded: %s" % fromheader_enc)

        # delete existing From header
        del msg["From"]
        # add new From header
        msg["From"] = fromheader_enc
        suspect.set_message_rep(msg)

        # There should be nothing found because only addresses with valid mail are reported
        receiver_list = suspect.parse_from_type_header(header="From")
        self.assertEqual(0, len(receiver_list))

        # There should be one address found with everything in displaypart
        receiver_list = suspect.parse_from_type_header(header="From", validate_mail=False, use_cache=False)
        self.assertEqual([(fromheader_raw, '')], receiver_list)

    def test_strip_attachments(self):
        """Test is attachment is stripped and therefore message smaller"""
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', TESTDATADIR + '/6mbzipattachment.eml')

        source_stripped_attachments = suspect.source_stripped_attachments()
        print("Size original/stripped message: %u/%u"
              % (len(suspect.get_original_source()), len(source_stripped_attachments)))
        self.assertTrue(len(source_stripped_attachments) < len(suspect.get_source()) / 10,
                        "after stripping zip attachment, size should be less than 10% of original mail")

    def test_strip_attachments_maxsize(self):
        """Test is attachment is stripped and max size applied"""
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', TESTDATADIR + '/6mbzipattachment.eml')

        source_stripped_attachments = suspect.source_stripped_attachments(maxsize=100)
        print("Size original/stripped message: %u/%u"
              % (len(suspect.get_original_source()), len(source_stripped_attachments)))
        self.assertEqual(100,len(source_stripped_attachments),
                        "after stripping zip attachment and limiting size, size should be 100")

    def test_msgrep_haederonly(self):
        """Test routine returning special headeronly python msg representation"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # check headers just set
        msg = suspect.build_headeronly_message_rep()

        # 10 headers
        self.assertIs(10, len(msg))
        
        if hasattr(msg, 'get_body'):
            # there shouldn't be a body:
            self.assertIsNone(msg.get_body())

    def test_stripspaces(self):
        """Test stripping spaces from left & right of sender/recipients"""
        sender = "sender@unittests.fuglu.org"
        recipients = ["recipient1@unittest.fuglu.org", "recipient@unittest.fuglu.org"]
        suspect = Suspect(f'  {sender}  ', [f"  {rec}  " for rec in recipients], '/dev/null')
        self.assertEqual(sender, suspect.from_address)
        self.assertEqual(sorted(recipients), sorted(suspect.recipients))


class SuspectFilterTestCase(unittest.TestCase):

    """Test Suspectfilter"""

    def setUp(self):
        self.candidate = SuspectFilter(TESTDATADIR + '/headertest.regex')

    def tearDown(self):
        pass

    def test_sf_get_args(self):
        """Test SuspectFilter files"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        suspect.tags['testtag'] = 'testvalue'

        headermatches = self.candidate.get_args(suspect)
        self.assertTrue(
            'Sent to unittest domain!' in headermatches, "To_domain not found in headercheck")
        self.assertTrue('Envelope sender is sender@unittests.fuglu.org' in headermatches,
                        "Envelope Sender not matched in header chekc")
        self.assertTrue('Mime Version is 1.0' in headermatches,
                        "Standard header Mime Version not found")
        self.assertTrue(
            'A tag match' in headermatches, "Tag match did not work")
        self.assertTrue(
            'Globbing works' in headermatches, "header globbing failed")
        self.assertTrue(
            'body rule works' in headermatches, "decoded body rule failed")
        self.assertTrue(
            'full body rule works' in headermatches, "full body failed")
        self.assertTrue('mime rule works' in headermatches, "mime rule failed")
        self.assertFalse('this should not match in a body rule' in headermatches,
                         'decoded body rule matched raw body')

        # perl style advanced rules
        self.assertTrue('perl-style /-notation works!' in headermatches,
                        "new rule format failed: %s" % headermatches)
        self.assertTrue('perl-style recipient match' in headermatches,
                        "new rule format failed for to_domain: %s" % headermatches)
        self.assertFalse('this should not match' in headermatches,
                         "rule flag ignorecase was not detected")

        # TODO: raw body rules

    def test_sf_matches(self):
        """Test SuspectFilter extended matches"""

        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        match, info = self.candidate.matches(suspect, extended=True)
        self.assertTrue(match, 'Match should return True')
        self.assertEqual(info.fieldname, 'to_domain')
        self.assertEqual(info.value, 'unittests.fuglu.org')
        self.assertEqual(info.arg, 'Sent to unittest domain!')
        self.assertEqual(info.pattern, r'unittests\.fuglu\.org')

    def test_sf_get_field(self):
        """Test SuspectFilter field extract"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # additional field tests
        self.assertEqual(self.candidate.get_field(
            suspect, 'clienthelo')[0], 'helo1')
        self.assertEqual(self.candidate.get_field(
            suspect, 'clientip')[0], '10.0.0.1')
        self.assertEqual(self.candidate.get_field(
            suspect, 'clienthostname')[0], 'rdns1')

        #--------------------------------#
        #- testing input & return types -#
        #--------------------------------#

        #--
        # headers
        #--
        get_field_return = self.candidate.get_field( suspect,'Received')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(4,len(get_field_return),"Number of received headers has to match the helloworld.eml example")
        for item in get_field_return:
            self.assertEqual(stringtype,type(item),"List element returned by get_field has to be unicode")

        get_field_return_u = self.candidate.get_field( suspect,'Received')
        self.assertEqual(get_field_return,get_field_return_u,"Unicode input to get_field should not change output")

        get_field_return_b = self.candidate.get_field( suspect,b'Received')
        self.assertEqual(get_field_return,get_field_return_b,"Bytes input to get_field should not change output")

        #--
        # envelope data
        #--
        get_field_return = self.candidate.get_field( suspect, 'clienthelo')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(1,len(get_field_return),"get_field on envelope data should return a list containing only 1 element")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")

        get_field_return = self.candidate.get_field( suspect, b'clienthelo')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(1,len(get_field_return),"get_field on envelope data should return a list containing only 1 element")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")

        get_field_return = self.candidate.get_field( suspect, 'clienthelo')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(1,len(get_field_return),"get_field on envelope data should return a list containing only 1 element")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")

        #--
        # tags
        #--
        suspect.tags['testtag' ] = 'testvalue'
        suspect.tags['testtagb'] = b'testvalue'
        suspect.tags['testtagu'] = 'testvalue'

        get_field_return = self.candidate.get_field(suspect, '@testtag')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")
        self.assertEqual(force_uString(suspect.tags['testtagb']),get_field_return[0])

        get_field_return = self.candidate.get_field(suspect, '@testtagb')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")
        self.assertEqual(force_uString(suspect.tags['testtagb']),get_field_return[0])

        get_field_return = self.candidate.get_field(suspect, '@testtagu')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")
        self.assertEqual(force_uString(suspect.tags['testtagu']),get_field_return[0])

        #--
        # body rules
        #--
        get_field_return = self.candidate.get_field(suspect, 'body:raw')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(1,len(get_field_return),"get_field for body:raw should return a list containing only 1 element")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")

        get_field_returnb = self.candidate.get_field(suspect, b'body:raw')
        self.assertEqual(get_field_return,get_field_returnb,"get_field output has to be the same independent of headername input type")
        get_field_returnu = self.candidate.get_field(suspect, 'body:raw')
        self.assertEqual(get_field_return,get_field_returnu,"get_field output has to be the same independent of headername input type")

        get_field_return = self.candidate.get_field(suspect, 'body')
        self.assertEqual(list,type(get_field_return),"Return type of get_field has to be a list")
        self.assertEqual(1,len(get_field_return),"get_field for body should return a list containing only 1 element")
        self.assertEqual(stringtype,type(get_field_return[0]),"List element returned by get_field has to be unicode")

        get_field_returnb = self.candidate.get_field(suspect, b'body')
        self.assertEqual(get_field_return,get_field_returnb,"get_field output has to be the same independent of headername input type")
        get_field_returnu = self.candidate.get_field(suspect, 'body')
        self.assertEqual(get_field_return,get_field_returnu,"get_field output has to be the same independent of headername input type")

    def test_strip(self):
        html = """foo<a href="bar">bar</a><script language="JavaScript">echo('hello world');</script>baz"""
        htmlu=u"""foo<a href="bar">bar</a><script language="JavaScript">echo('hello world');</script>baz"""
        htmlb=b"""foo<a href="bar">bar</a><script language="JavaScript">echo('hello world');</script>baz"""

        declarationtest = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
  <head>
    <title>greetings</title>
  </head>
  <body>
    <font color="red">well met!</font>
  </body>
</html>
"""
        # word generated empty message
        wordhtml = """<html xmlns:v=3D"urn:schemas-microsoft-com:vml"
xmlns:o=3D"urn:schemas-microsoft-com:office:office"
xmlns:w=3D"urn:schemas-microsoft-com:office:word"
xmlns:m=3D"http://schemas.microsoft.com/office/2004/12/omml"
xmlns=3D"http://www.w3.org/TR/REC-html40"><head><META
HTTP-EQUIV=3D"Content-Type" CONTENT=3D"text/html;
charset=3Dus-ascii"><meta name=3DGenerator content=3D"Microsoft Word 15
(filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-language:EN-US;}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:#0563C1;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:#954F72;
	text-decoration:underline;}
span.E-MailFormatvorlage17
	{mso-style-type:personal-compose;
	font-family:"Calibri",sans-serif;
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri",sans-serif;
	mso-fareast-language:EN-US;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:70.85pt 70.85pt 2.0cm 70.85pt;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext=3D"edit" spidmax=3D"1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext=3D"edit">
<o:idmap v:ext=3D"edit" data=3D"1" />
</o:shapelayout></xml><![endif]--></head><body lang=3DDE-CH
link=3D"#0563C1" vlink=3D"#954F72"><div class=3DWordSection1><p
class=3DMsoNormal><o:p> </o:p></p></div></body></html>"""

        for use_bfs in [True, False]:
            stripped = self.candidate.strip_text(html, use_bfs=use_bfs)
            self.assertEqual(stripped, 'foobarbaz')

            docstripped = self.candidate.strip_text(
                declarationtest, use_bfs=use_bfs)
            self.assertEqual(
                docstripped.split(), ['greetings', 'well', 'met!'])

            wordhtmstripped = self.candidate.strip_text(
                wordhtml, use_bfs=use_bfs)
            self.assertEqual(wordhtmstripped.strip(), '')

        # check input type conversions and return type
        remove_tagsu = ['script', 'style']
        remove_tagsb = [b'script', b'style']
        stripped = self.candidate.strip_text(htmlu,remove_tags=remove_tagsu)
        self.assertEqual(stringtype,type(stripped))
        self.assertEqual('foobarbaz',stripped)
        stripped = self.candidate.strip_text(htmlu,remove_tags=remove_tagsb)
        self.assertEqual(stringtype,type(stripped))
        self.assertEqual('foobarbaz',stripped)
        stripped = self.candidate.strip_text(htmlb,remove_tags=remove_tagsb)
        self.assertEqual(stringtype,type(stripped))
        self.assertEqual('foobarbaz',stripped)
        stripped = self.candidate.strip_text(htmlb,remove_tags=remove_tagsu)
        self.assertEqual(stringtype,type(stripped))
        self.assertEqual('foobarbaz',stripped)

    def test_sf_get_decoded_textparts(self):
        """Test return type for Python 2/3 consistency (list of unicode strings)"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        textpartslist = self.candidate.get_decoded_textparts(suspect)
        self.assertEqual(list,type(textpartslist),"return type has to be list of unicode strings, but it's not a list")
        self.assertEqual(1,len(textpartslist),"for given example there is one text part, therefore list size has to be 1")
        self.assertEqual(stringtype,type(textpartslist[0]),"return type has to be list of unicode strings, but list doesn't contain a unicode string")

    def test_sf_get_decoded_textparts_cache(self):
        """Test enabled (default) cache for the decoded text buffers"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        textpartslist  = self.candidate.get_decoded_textparts(suspect)
        textpartslist2 = self.candidate.get_decoded_textparts(suspect)
        self.assertEqual(id(textpartslist[0]),id(textpartslist2[0]),"with caching the same object should be returned")

    def test_sf_get_decoded_textparts_nocache(self):
        """Test disabled cache for the decoded text buffers"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')

        # disable caching for "decoded_buffer_text" property
        for obj in suspect.att_mgr.get_objectlist():
            obj.set_cachelimit("decoded_buffer_text","nocache",True)

        textpartslist  = self.candidate.get_decoded_textparts(suspect)
        textpartslist2 = self.candidate.get_decoded_textparts(suspect)
        self.assertNotEqual(id(textpartslist[0]),id(textpartslist2[0]),"with no caching there should be different objects returned")

    def test_textparts_noattachments(self):
        """Extract text parts which are not attachements"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/lorem_attached.eml')

        # no attached textparts
        textpartslist = self.candidate.get_decoded_textparts(suspect, attachment=False)
        print(textpartslist)
        # <html>\r\n
        #    <head>\r\n
        #        <meta http-equiv="content-type" content="text/html; charset=UTF-8">\r\n
        #    </head>\r\n
        #    <body text="#000000" bgcolor="#FFFFFF">\r\n
        #        <p>body text<br>\r\n
        #        </p>\r\n
        #    </body>\r\n
        # </html>\r\n
        self.assertEqual(1, len(textpartslist))
        self.assertTrue("body text<br>" in textpartslist[0])

    def test_textparts_attachments(self):
        """Extract text parts which are attachements"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/lorem_attached.eml')

        # only attached textparts
        textpartslist = self.candidate.get_decoded_textparts(suspect, attachment=True)
        print(textpartslist)
        self.assertEqual(1, len(textpartslist))
        self.assertTrue("Lorem ipsum" in textpartslist[0])

    def test_textparts_no_attachments(self):
        """Extract text parts which are not attachements"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/lorem_inline.eml')

        # no attached textparts
        textpartslist = self.candidate.get_decoded_textparts(suspect, attachment=False)
        print(textpartslist)
        self.assertEqual(2, len(textpartslist))
        self.assertTrue("body text<br>" in textpartslist[0] or "body text<br>" in textpartslist[1])
        self.assertTrue("Lorem ipsum" in textpartslist[0] or "Lorem ipsum" in textpartslist[1])

    def test_textparts_only_attachments(self):
        """Extract text parts which are attachements"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/lorem_inline.eml')

        # no attached textparts
        textpartslist = self.candidate.get_decoded_textparts(suspect, attachment=True)
        print(textpartslist)
        self.assertEqual(0, len(textpartslist))

    def test_textparts_inline(self):
        """Extract text parts which are inline. Note the difference to test_textparts_no_attachments."""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/lorem_inline.eml')

        # no attached textparts
        textpartslist = self.candidate.get_decoded_textparts(suspect, inline=True)
        print(textpartslist)
        self.assertEqual(1, len(textpartslist))
        self.assertTrue("Lorem ipsum" in textpartslist[0])
    
    
    def test_parse_rules(self):
        result = self.candidate._load_simplestyle_line('field regex comment')
        self.assertIsNotNone(result, 'failed to parse simplestyle line')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_simplestyle_line('field regex')
        self.assertIsNotNone(result, 'failed to parse simplestyle line without comment')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, None)
        
        result = self.candidate._load_simplestyle_line('field: regex comment')
        self.assertIsNotNone(result, 'failed to parse simplestyle line with colon')
        self.assertEqual(result.fieldname, 'field') # no colon
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_simplestyle_line('@field regex comment')
        self.assertIsNotNone(result, 'failed to parse simplestyle line with @field')
        self.assertEqual(result.fieldname, '@field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_simplestyle_line('field >0 comment')
        self.assertIsNotNone(result, 'failed to parse simplestyle line with comparison')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.op, operator.gt)
        self.assertEqual(result.num, 0)
        self.assertEqual(result.cmp, '>0')
        self.assertEqual(result.args, 'comment')
        
        with self.assertRaises(ValueError, msg='somehow parse simplestyle line with invalid comparison'):
            self.candidate._load_simplestyle_line('field >>0 comment')
        
        result = self.candidate._load_perlstyle_line('field /regex/ comment')
        self.assertIsNotNone(result, 'failed to parse perlstyle line')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_perlstyle_line('field /regex/')
        self.assertIsNotNone(result, 'failed to parse perlstyle line without comment')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, None)
        
        result = self.candidate._load_perlstyle_line('field: /regex/ comment')
        self.assertIsNotNone(result, 'failed to parse perlstyle line with field:')
        self.assertEqual(result.fieldname, 'field') # no colon
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_perlstyle_line('@field /regex/ comment')
        self.assertIsNotNone(result, 'failed to parse perlstyle line with @field')
        self.assertEqual(result.fieldname, '@field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_perlstyle_line('field /regex/i comment')
        self.assertIsNotNone(result, 'failed to parse perlstyle line with flagged rgx')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_perlstyle_line('field regex comment')
        self.assertIsNone(result, 'somehow parsed perlstyle line with invalid/unmarked rgx')
        
        result = self.candidate._load_perlstyle_line('field /regex comment')
        self.assertIsNone(result, 'somehow parsed perlstyle line with invalid/unmarked rgx')
        
        result = self.candidate._load_perlstyle_line('field regex/ comment')
        self.assertIsNone(result, 'somehow parsed perlstyle line with invalid/unmarked rgx')
        
        result = self.candidate._load_perlstyle_line('field regex/i comment')
        self.assertIsNone(result, 'somehow parsed perlstyle line with invalid/unmarked rgx')
        
        result = self.candidate._load_jsonlines_line('{"field":"field", "regex":"regex", "flags":"i", "args":"comment"}')
        self.assertIsNotNone(result, 'failed to parse jsonline line with flagged rgx')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.pattern.pattern, 'regex')
        self.assertEqual(result.args, 'comment')
        
        result = self.candidate._load_jsonlines_line('{"field":"field", "cmp":">", "num":"0", "args":"comment"}')
        self.assertIsNotNone(result, 'failed to parse jsonline line with cmp')
        self.assertEqual(result.fieldname, 'field')
        self.assertEqual(result.op, operator.gt)
        self.assertEqual(result.num, 0)
        self.assertEqual(result.cmp, '>0')
        self.assertEqual(result.args, 'comment')
        
        with self.assertRaises(ValueError, msg='somehow parsed incomplete jsonline'):
            self.candidate._load_jsonlines_line('{"field":"field", "args":"comment"}')
        with self.assertRaises(ValueError, msg='somehow parsed broken jsonline'):
            self.candidate._load_jsonlines_line('{"field":"field", "regex":"regex", "args":"comment}')


class ActionCodeTestCase(unittest.TestCase):

    def test_defaultcodes(self):
        """test actioncode<->string conversion"""
        conf = ConfigParser()
        conf.add_section('spam')
        conf.add_section('virus')
        conf.set('spam', 'defaultlowspamaction', 'REJEcT')
        conf.set('spam', 'defaulthighspamaction', 'REjECT')
        conf.set('virus', 'defaultvirusaction', 'rejeCt')
        self.assertEqual(
            string_to_actioncode('defaultlowspamaction', conf), REJECT)
        self.assertEqual(
            string_to_actioncode('defaulthighspamaction', conf), REJECT)
        self.assertEqual(
            string_to_actioncode('defaultvirusaction', conf), REJECT)
        self.assertEqual(string_to_actioncode('nonexistingstuff'), None)
        self.assertEqual(actioncode_to_string(REJECT), 'REJECT')
        self.assertEqual(
            actioncode_to_string(string_to_actioncode('discard')), 'DELETE')


class TemplateTestcase(unittest.TestCase):

    """Test Templates"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_template(self):
        """Test Basic Template function"""

        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        suspect.tags['nobounce'] = True

        reason = "a three-headed monkey stole it"

        template = """Your message '${subject}' from ${from_address} to ${to_address} could not be delivered because ${reason}"""

        result = apply_template(template, suspect, dict(reason=reason))
        expected = """Your message 'Hello world!' from sender@unittests.fuglu.org to recipient@unittests.fuglu.org could not be delivered because a three-headed monkey stole it"""
        self.assertEqual(
            result, expected), "Got unexpected template result: %s" % result


    def test_tag_templates(self):
        """Test Templates with suspect tag expansion"""

        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        suspect.tags['hello'] = ['World', "is it me you're looking for"]
        suspect.tags['SAPlugin.spamscore']="13.37"

        def valfunc(data):
            if '@removeme' in data:
                del data['@removeme']
            if '@changeme' in data:
                data['@changeme']='elephant'
            return data

        today=str(datetime.date.today())
        suspect.tags['removeme']='disappearing rabbit'
        suspect.tags['changeme']='an uninteresting value'
        cases=[
            ('${subject}','Hello world!'),
            ('${@SAPlugin.spamscore}','13.37'),
            ('${blubb}',''),
            ('${@hello}','World'),
            ('${date}',today), #builtin function with same name as header: builtin should win
            ('${header:date}', 'Tue, 12 Nov 2013 01:12:11 +0200'),  # with explicit header: prefix the message header should be available
            ('The quick brown ${from_address} received on ${date} jumps over the ${@removeme}. Uh ${@changeme}', 'The quick brown sender@unittests.fuglu.org received on %s jumps over the . Uh elephant'%today),
        ]
        for c in cases:
            template,expected = c
            result = apply_template(template, suspect, valuesfunction=valfunc)
            self.assertEqual(result, expected), "Got unexpected template result: %s. Should be: %s" % (result,expected)


class ClientInfoTestCase(unittest.TestCase):

    """Test client info detection"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_parse_rcvd_header(self):
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', '/dev/null')
        self.assertEqual(suspect._parse_rcvd_header(
            "from helo1 (rdns1 [10.0.0.1])")[:3], ('helo1', 'rdns1', '10.0.0.1'))
        self.assertEqual(suspect._parse_rcvd_header("from mx0.slc.paypal.com (mx1.slc.paypal.com [173.0.84.226])")[:3], (
            'mx0.slc.paypal.com', 'mx1.slc.paypal.com', '173.0.84.226'))
        self.assertEqual(suspect._parse_rcvd_header("from mail1a.tilaa.nl (mail1a.tilaa.nl [IPv6:2a02:2770:6:0:21a:4aff:fea8:1328])")[:3], (
            'mail1a.tilaa.nl', 'mail1a.tilaa.nl', '2a02:2770:6:0:21a:4aff:fea8:1328'))

    def test_client_info(self):
        """Test client info using eml file"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        helo, ip, revdns = suspect.client_info_from_rcvd(None, 0)
        self.assertEqual(helo, 'helo1')
        self.assertEqual(ip, '10.0.0.1')
        self.assertEqual(revdns, 'rdns1')

        helo, ip, revdns = suspect.client_info_from_rcvd(None, 1)
        self.assertEqual(helo, 'helo2')
        self.assertEqual(ip, '10.0.0.2')
        self.assertEqual(revdns, 'rdns2')

        helo, ip, revdns = suspect.client_info_from_rcvd(r'10\.0\.0\.2', 1)
        self.assertEqual(helo, 'helo3')
        self.assertEqual(ip, '10.0.0.3')
        self.assertEqual(revdns, 'rdns3')

    def test_utf8_received(self):
        """Test parsing received header with utf8 char"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/nonascii_env_rcpt.eml')
        helo, ip, revdns = suspect.client_info_from_rcvd(None, 0)
        self.assertEqual(helo, 'dcba.gfedcba.aa')
        self.assertEqual(ip, '10.0.0.1')
        self.assertEqual(revdns, 'abcd.abcdefg.aa')


class FileListTestCase(unittest.TestCase):

    def setUp(self):
        testdata = """CASE?
{whitespace}stripped ?{whitespace}
{whitespace}

{whitespace}# no comment!
""".format(whitespace='    ')
        self.filename = '/tmp/fuglufilelisttest.txt'
        open(self.filename, 'w').write(testdata)

    def tearDown(self):
        os.unlink(self.filename)

    def test_filelist(self):
        self.assertEqual(FileList(filename=self.filename, strip=True, skip_empty=True, skip_comments=True,
                                  lowercase=False, additional_filters=None).get_list(), ['CASE?', 'stripped ?'])
        self.assertEqual(FileList(filename=self.filename, strip=False, skip_empty=True, skip_comments=True,
                                  lowercase=False, additional_filters=None).get_list(), ['CASE?', '    stripped ?    ', '    '])
        self.assertEqual(FileList(filename=self.filename, strip=True, skip_empty=False, skip_comments=False,
                                  lowercase=False, additional_filters=None).get_list(), ['CASE?', 'stripped ?', '', '', '# no comment!'])
        self.assertEqual(FileList(filename=self.filename, strip=True, skip_empty=True, skip_comments=True,
                                  lowercase=True, additional_filters=None).get_list(), ['case?', 'stripped ?'])

class StaticFunctionTests(unittest.TestCase):
    """Unit tests for static functions"""

    @staticmethod
    def old_add_header_use_python_mail(content, header_name, header_value):
        msgrep = email.message_from_bytes(content)
        msgrep.add_header(header_name, header_value)
        msg = msgrep.as_bytes()
        return msg

    def test_prepend_ascii_header_to_source(self):
        """Test header with a simple us-ascii value"""

        source = b""
        expected = b'fancy-test-header: Fancy Value\r\n'
        outsource = Suspect.prepend_header_to_source("fancy-test-header", "Fancy Value", source)
        self.assertEqual(expected, outsource)

    def test_prepend_header_to_source(self):
        """Test header with a value that has to be encoded"""
        source = b""
        #expected = b'fancy-test-header: =?utf-8?b?RsOkbnNpIFbDpGxqdQ==?=\r\n' # generated by Header
        expected = b'fancy-test-header: =?utf-8?q?F=C3=A4nsi_V=C3=A4lju?=\r\n' # generated by HeaderRegistry
        outsource = Suspect.prepend_header_to_source("fancy-test-header", "Fänsi Välju", source)
        self.assertEqual(expected, outsource)


    def test_header_with_python_email(self):
        """Compare result to the test method converting mail first to python email object"""
        inputfile = TESTDATADIR + '/helloworld.eml'
        msg_bstring = open(inputfile, 'rb').read()
        
        expected_new = b'fancy-test-header: =?utf-8?q?F=C3=A4nsi_V=C3=A4lju?=\r\n'  # generated by HeaderRegistry
        expected_old = b'fancy-test-header: =?utf-8?b?RsOkbnNpIFbDpGxqdQ==?=' # generated by Header

        source1 = Suspect.prepend_header_to_source("fancy-test-header", "Fänsi Välju", msg_bstring)
        source2 = StaticFunctionTests.old_add_header_use_python_mail(msg_bstring, "fancy-test-header", "Fänsi Välju")

        self.assertIn(expected_new, source1)
        self.assertIn(expected_old, source2)

        print(source1[:20])
        print(source2[:20])

        index1 = source1.index(expected_new)
        index2 = source2.index(expected_old)
        print("position prepend: %u, position python.email: %u" % (index1, index2))

    def test_prepend_to_corrupted_mail(self):
        """Corrupted mail, still it should be possible to prepend a header"""
        inputfile = TESTDATADIR + '/new_ascii_error.eml'
        msg_bstring = open(inputfile, 'rb').read()

        expected = b'fancy-test-header: Fancy Value'

        source1 = Suspect.prepend_header_to_source("fancy-test-header", "Fancy Value", msg_bstring)
        self.assertIn(expected, source1)
        with self.assertRaises(UnicodeEncodeError, msg="Test can be changed if newer Python "
                                                       "version don't create an exception anymore"):
            source2 = StaticFunctionTests.old_add_header_use_python_mail(msg_bstring,
                                                                         "fancy-test-header",
                                                                         "Fancy Value")
    def test_config_commaseplist_nospace(self):
        """Test extraction for comma separated list from string(config)"""
        mylist = ["body.uris", "header.uris", "custom.uris"]
        inputstring = ",".join(mylist)
        outlist = Suspect.getlist_space_comma_separated(inputstring=inputstring)
        self.assertEqual(mylist, outlist)

    def test_config_spaceseparated(self):
        """Test extraction for space separated list from string(config)"""
        mylist = ["body.uris", "header.uris", "custom.uris"]
        inputstring = " ".join(mylist)
        outlist = Suspect.getlist_space_comma_separated(inputstring=inputstring)
        self.assertEqual(mylist, outlist)

    def test_config_comma_space_seplist(self):
        """Test extraction for comma + space separated list from string(config)"""
        mylist = ["body.uris", "header.uris", "custom.uris"]
        inputstring = ", ".join(mylist)
        outlist = Suspect.getlist_space_comma_separated(inputstring=inputstring)
        self.assertEqual(mylist, outlist)

    def test_config_comma_space_seplist_encoded(self):
        """
        Test extraction for comma + space separated list from string(config)
        with additional encoded chars (space, comma, backslash)
        """
        mylist_in = ["body\\\\uris", "header\\,uris", "custom\\ uris"]
        mylist    = ["body\\uris", "header,uris", "custom uris"]
        inputstring = ", ".join(mylist_in)
        outlist = Suspect.getlist_space_comma_separated(inputstring=inputstring)
        self.assertEqual(mylist, outlist)


class TestSuspectClientInfo(unittest.TestCase):
    """Suspect routines testing received header parsing"""

    headers = """Received: from aaaaaaaaa.aaaaaa.aaa ([192.168.0.100])
\tby aaaaaa-aaaaa.aaaaaa.aaa (Dovecot) with LMTP id AAAAAAAAAAAAAAAAAAAAAA
\t; Wed, 01 Apr 2020 00:00:00 +0000"
Received: from aaaaaaaa-aaaadd.aaaaaaa.aa (10.0.0.1) by aaaadd.aa.aaaa.aa (0.0.000)
\tid AAAAAAAAAAAAAAAA for aaaa-aaaaaaaa.aa@aaaaaaa.aa; Wed, 1 Apr 2020 00:00:00 +0000
Received: from aaaaaaaa-aaaadd.aaaaaaa.aa ([10.0.0.3])
\tby aaaaa.aaaaaaaa.aa ([10.0.0.4])
\twith ESMTP via TCP; 01 Apr 2020 00:00:00 -0000


"""

    headers2 = """
Received: from aa-dd.fuglu.org (aa-dd.fuglu.org [192.168.1.37])
    by aaa-aaa-dd.fuglu.org (FUGLU ORG) with ESMTP id XXXXXXXXXXX
    for <recipient@fuglu.org> Wed, 2 Sep 1920 00:00:00 +0200 (CEST)
Received: from aa-dd.fuglu.org (localhost.localdomain [127.0.0.1])
    by aa-dd.fuglu.org (FUGLU ORG) with SMTP id XXXXXXXXXX
    for <recipient@fuglu.org> Wed, 2 Sep 1920 00:00:00 +0200 (CEST)
Received: from firstexternal.aaaaaa-aaaaaaa.aa (firstexternal.aaaaaa-aaaaaaa.aa [192.168.1.28])
    by aa-dd.fuglu.org (FUGLU ORG) with ESMTPS id XXXXXXXXXX
    for <recipient@fuglu.org> Wed, 2 Sep 1920 00:00:00 +0200 (CEST)
Received: by aaaddd.aaaaaa-aaaaaaa.aa (aaaaaa.aaaa, from userid 0)
    id XXXXXXXXXXX; Wed, 2 Sep 2020 00:00:00 +0200 (CEST)
    
    
    
    """.lstrip()

    def test_parse_rcv_header(self):
        """Test rcv header with rdns and auth"""
        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        header = "from aaaaaaa-aaa-aa-aaaaaaaa-d-d.aaaa (aaaa-aaa-d-dd-ddd.add-ddd.aaa.aaaaaaa.aa [192.168.0.1])"\
                 "    (Authenticated sender: aaaaaaaa@aaaaaa.aa)"\
                 "    by aaaaaaaad.aaaaaa.aaa (Postfix) with ESMTPSA id ddaaaadaadadaa;"\
                 "    Wed, 1 Apr 2020 00:00:00 +0200 (CEST)"
        helo, revdns, ip = s._parse_rcvd_header(header)[:3]

        self.assertEqual(
            ("aaaaaaa-aaa-aa-aaaaaaaa-d-d.aaaa", "aaaa-aaa-d-dd-ddd.add-ddd.aaa.aaaaaaa.aa", '192.168.0.1'),
            (helo, revdns, ip)
        )

    def test_parse_rcv_header_noip(self):
        """Test case where no clientinfo can be extracted (no square brackets, no ip)"""
        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        header = "from aaaaaadd.aaaaaaaaa.aa"\
                 "         by aaaaaadd.aaaaaaaaa.aa with LMTP"\
                 "         id AAAAAAAAAAAAAAAAAAAAAA"\
                 "         (envelope-from <aaaaaa-aa.aad_dddddd.ddddd-ddaadddddd@aaaadd.aaaddd.aaaaa.aaa>)"\
                 "         for <aaaaaaa.aaaaaa@aaaaaaaa.aa>; Wed, 01 Apr 2020 00:00:00 +0200"
        clientinfo = s._parse_rcvd_header(header)
        self.assertIsNone(clientinfo)

    def test_parse_rcv_header_nordns(self):
        """Test rcv header without rdns"""
        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        header = "from aaaaaaaaa.aaaaaa.aaa ([192.168.0.100]) " \
                 "by aaaaaa-aaaaa.aaaaaa.aaa (Dovecot) with LMTP id AAAAAAAAAAAAAAAAAAAAAA" \
                 "; Wed, 01 Apr 2020 00:00:00 +0000"
        helo, revdns, ip = s._parse_rcvd_header(header)[:3]

        self.assertEqual(
            ('aaaaaaaaa.aaaaaa.aaa', None, '192.168.0.100'),
            (helo, revdns, ip)
        )

    def test_parse_rcv_header__nordns_ml(self):
        """Test multiline rcv header with tabs"""
        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        header = "from aaaaaaaaa.aaaaaa.aaa ([192.168.0.100])\n" \
                 "\tby aaaaaa-aaaaa.aaaaaa.aaa (Dovecot) with LMTP id AAAAAAAAAAAAAAAAAAAAAA\n" \
                 "\t; Wed, 01 Apr 2020 00:00:00 +0000"
        helo, revdns, ip = s._parse_rcvd_header(header)[:3]

        self.assertEqual(
            ('aaaaaaaaa.aaaaaa.aaa', None, '192.168.0.100'),
            (helo, revdns, ip)
        )

    def test_clientinfo_sip_serr(self):
        """Test clientinfo extraction with ip skip and error skip"""
        conf = ConfigParser()
        conf.add_section('environment')
        conf.set('environment', 'trustedhostsregex', r'^(192\.168\.)')
        conf.set('environment', 'boundarydistance', '0')
        conf.set('environment', 'skiponerror', 'true')
        # 1) The first header should be skipped because it contains an ip in trustedhostsregex
        # 2) The second header is skipped because there's no clientinfo extracted (no square brackets)
        #    and 'skiponerror' is True in the config
        # 3) Information is extracted from here

        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        s.set_source(force_bString(self.headers))

        clientinfo = s.get_client_info(config=conf)
        self.assertEqual(('aaaaaaaa-aaaadd.aaaaaaa.aa', '10.0.0.3', None), clientinfo)

    def test_clientinfo_sip_err(self):
        """Test clientinfo extraction with ip skip and no error skip"""
        conf = ConfigParser()
        conf.add_section('environment')
        conf.set('environment', 'trustedhostsregex', r'^(192\.168\.)')
        conf.set('environment', 'boundarydistance', '0')
        conf.set('environment', 'skiponerror', 'false')
        # 1) The first header should be skipped because it contains an ip in trustedhostsregex
        # 2) The second header creates an error because no clientinfo can be extracted (no square brackets).
        #    Because 'skiponerror' is False in the config the clientinfo extraction ends end returns None

        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        s.set_source(force_bString(self.headers))

        clientinfo = s.get_client_info(config=conf)
        self.assertIsNone(clientinfo)

    def test_clientinfo_ip(self):
        """Test clientinfo extraction without ip skip"""
        conf = ConfigParser()
        conf.add_section('environment')
        conf.set('environment', 'trustedhostsregex', r'^(192\.168\.1\.)')
        conf.set('environment', 'boundarydistance', '0')
        conf.set('environment', 'skiponerror', 'false')
        # 1) The first header should be extracted

        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        s.set_source(force_bString(self.headers))

        clientinfo = s.get_client_info(config=conf)
        self.assertEqual(('aaaaaaaaa.aaaaaa.aaa', '192.168.0.100', None), clientinfo)

    def test_clientinfo_sip_err_def(self):
        """Test clientinfo extraction with ip skip and no error skip as default option"""
        conf = ConfigParser()
        conf.add_section('environment')
        conf.set('environment', 'trustedhostsregex', r'^(192\.168\.)')
        conf.set('environment', 'boundarydistance', '0')
        # 1) The first header should be skipped because it contains an ip in trustedhostsregex
        # 2) The second header creates an error because no clientinfo can be extracted (no square brackets).
        #    Because 'skiponerror' is False in the config the clientinfo extraction ends end returns None
        #    -> skiponerror=False is taken as default option, otherwise test is equal to 'test_clientinfo_sip_err'

        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        s.set_source(force_bString(self.headers))

        clientinfo = s.get_client_info(config=conf)
        self.assertIsNone(clientinfo)

    def test_ignore_lmtp(self):
        """Ignore LMTP line using trustedreceivedregex"""
        """Test clientinfo extraction with ip skip and error skip"""
        conf = ConfigParser()
        conf.add_section('environment')
        conf.set('environment', 'trustedhostsregex', '')
        conf.set('environment', 'trustedreceivedregex', '(with LMTP id)')
        conf.set('environment', 'boundarydistance', '0')
        conf.set('environment', 'skiponerror', 'true')
        # 1) The first header should be skipped because it contains is an LMTP transaction skipped by  in trustedreceivedregex
        # 2) The second header is skipped because there's no clientinfo extracted (no square brackets)
        #    and 'skiponerror' is True in the config
        # 3) Information is extracted from here

        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        s.set_source(force_bString(self.headers))

        clientinfo = s.get_client_info(config=conf)
        self.assertEqual(('aaaaaaaa-aaaadd.aaaaaaa.aa', '10.0.0.3', None), clientinfo)

    def test_ignore_samedomain(self):
        """Ignore same domain"""
        """Test clientinfo extraction with ip skip and error skip"""
        conf = ConfigParser()
        conf.add_section('environment')
        conf.set('environment', 'trustedhostsregex', '')
        conf.set('environment', 'boundarydistance', '0')
        conf.set('environment', 'skipsamedomain', 'true')
        # 1) the first received header is skipped because helo & rdns domains are the same as the receivec-by domain
        # 2) the second received header is skipped because helo  domain is the same as the receivec-by domain
        # 3) Information is extracted from here

        s = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        s.set_source(force_bString(self.headers2))

        clientinfo = s.get_client_info(config=conf)
        self.assertEqual(('firstexternal.aaaaaa-aaaaaaa.aa', '192.168.1.28', 'firstexternal.aaaaaa-aaaaaaa.aa'),
                         clientinfo)


class TestScannerPluginOverride(unittest.TestCase):
    """Test return code override option for scanner plugins"""

    class DummyScannerPlugin(ScannerPlugin):
        """A dummy scanner plugin"""
        def __init__(self, config, section=None):
            super().__init__(config, section=section)

        def examine(self, suspect: Suspect):
            return REJECT

    def test_reject(self):
        """Test reject of dummy scanner plugin"""
        conf = ConfigParser()
        conf.add_section('test')

        plug = TestScannerPluginOverride.DummyScannerPlugin(conf, section="test")
        sus = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        out = plug.run_examine(suspect=sus)
        self.assertEqual(out, REJECT)

    def test_reject_override(self):
        """Test reject override of dummy scanner plugin"""
        conf = ConfigParser()
        conf.add_section('test')
        conf.set('test', 'overrideaction', 'dunno')

        plug = TestScannerPluginOverride.DummyScannerPlugin(conf, section="test")
        sus = Suspect('sender@example.com', 'recipient@example.com', '/dev/null')
        out = plug.run_examine(suspect=sus)
        self.assertEqual(out, DUNNO)


class TestHeaderMod(unittest.TestCase):
    def test_remove_headers(self):
        """Test removing multiple headers"""
        inputfile = TESTDATADIR + '/helloworld.eml'

        s = Suspect("from@fuglu.org", "to@fuglu.org", inputfile)
        msg = s.get_message_rep()
        msg.add_header("x-fuglu-myheader", "bli")
        msg.add_header("x-fuglu-myheader", "bla")
        msg.add_header("x-fuglu-myheader", "blubb")
        s.set_message_rep(msg)
        s.remove_headers("x-fuglu-myheader")
        msg = s.get_message_rep()
        self.assertNotIn("x-fuglu-myheader", msg.keys())
        self.assertIn("x-fuglu-myheader", s.removed_headers)
    
    def test_suspect_update(self):
        PREFIX = '[UPDATED] '
        def cb(value):
            return PREFIX + value
        
        subjectlines = {
            b'subject: simplesubject\r\n': ['[UPDATED] simplesubject'],
            b'Subject: =?utf-8?B?TmV1ZSBBdWZnYWJlIGltIEhSIENvcm5lciDCq0phaHJlc2dl?= =?utf-8?B?c3Byw6RjaCAyMDI0IC8gSGVsZW4gU2Now7ZuZW5iZXJnZXItV2ViZXLC?= =?utf-8?B?uw==?=\r\n':
                ['[UPDATED] Neue Aufgabe im HR Corner =?utf-8?q?=C2=ABJahresgespr?=\r\n =?utf-8?q?=C3=A4ch_2024_/_Helen_Sch=C3=B6nenberger-Weber=C2=BB?='],
            b'Subject: =?UTF-8?Q?Best=C3=A4tigungs-E-Mail=0A?=\r\n': ['[UPDATED] =?utf-8?q?Best=C3=A4tigungs-E-Mail=0A?='],
            b'Subject: =?UTF-8?Q?Best=C3=A4tigungs E-Mail=?UTF-8?Q?=0A?=\r\n': [
                '[UPDATED] =?utf-8?q?Best=C3=A4tigungs?= E-Mail=?UTF-8?Q0A?=', # legacy decode_header
                '[UPDATED] =?utf-8?q?Best=c3=a4tigungs E-Mail', # using email.parser
                '[UPDATED] =?UTF-8?Q?Best=C3=A4tigungs =?utf-8?q?E-Mail=0A?=', # new py >=3.11.11/3.12.5 using email.parser
            ],
            b'Subject: very long header that needs some folding somewhere but is otherwise very simple to handle\r\n':
                ['[UPDATED] very long header that needs some folding somewhere but is\r\n otherwise very simple to handle'],
            b'Subject: very long header that needs some folding =?UTF-8?Q?=0A?= and has a newline in the middle\r\n': [
                '[UPDATED] very long header that needs some folding =?utf-8?q?=0A?= and has a\r\n newline in the middle', # old py <=3.11.10/3.12.4
                '[UPDATED] very long header that needs some folding =?utf-8?q?=0A?=\r\n and has a newline in the middle', # new py >=3.11.11/3.12.5
            ],
            b'Subject: =?UTF-8?Q?Google_Ads=3A_Wir_haben_Ihre_Zahlung_f=C3=BCr_274=2D299=2D8311?= =?UTF-8?Q?=0D=0Aerhalten?=\r\n': [
                '[UPDATED] Google Ads: Wir haben Ihre Zahlung =?utf-8?q?f=C3=BCr?=\r\n 274-299-8311=?utf-8?q?=0D=0A?=erhalten', # old py <=3.11.10/3.12.4
                '[UPDATED] Google Ads: Wir haben Ihre Zahlung =?utf-8?q?f=C3=BCr_274?=\r\n =?utf-8?q?-299-8311=0D=0Aerhalten?=', # new py >=3.11.11/3.12.5
            ],
        }
        
        s = Suspect("from@fuglu.org", "to@fuglu.org", '/dev/null')
        for subjectline, exp_subjectline in subjectlines.items():
            src = b'X-Before: before\r\n before-continued\r\n'+subjectline+b'X-After: after\r\n after-continued\r\n\r\nbody\r\n'
            s.set_source(src)
            s.update_subject(cb) # generally we are happy if this does not raise exceptions
            v = s.get_header('subject')
            self.assertIsNotNone(v, f'no subject found in {s.get_source()}')
            self.assertTrue(v.startswith(PREFIX), f'subject does not start with prefix {PREFIX} in {s.get_source()}')
            self.assertIn(v.lower(), [x.lower() for x in exp_subjectline], f'mismatch in {subjectline} - expected {exp_subjectline} - got {v}')
            
            v = s.get_header('X-Before')
            self.assertEqual(v, 'before\r\n before-continued')
            v = s.get_header('X-After')
            self.assertEqual(v, 'after\r\n after-continued')

            # in result, subject is first because it has been prepended
            equals = False
            for es in exp_subjectline:
                tar = b"subject: " + es.encode() + b"\r\n" + b'X-Before: before\r\n before-continued\r\nX-After: after\r\n after-continued\r\n\r\nbody\r\n'
                msgsrc = s.get_source()
                equals = tar, msgsrc
            self.assertTrue(equals, f'source varies for subject {subjectline}')
    
    def test_broken_subj_decode_encode(self):
        # this subject is broken and would not decode properly using legacy email api
        src = b'''X-Start: headers
Subject: =?UTF-8?q?13-15_=D0=BD=D0=BE=D1=8F=D0=B1=D1=80=D1=8F_2024_=D0=92=D1=82?= =?UTF-8?q?=D0=BE=D1=80=D0=BE=D0=B9_=D0=90=D1=81=D1=82=D1=80=D0=B0=D1=85?= =?UTF-8?q?=D0=B0=D0=BD=D1=81=D0=BA=D0=B8=D0=B9_=D0=BC=D0=B5=D0=B6=D0=B4?= =?UTF-8?q?=D1=83=D0=BD=D0=B0=D1=80=D0=BE=D0=B4=D0=BD=D1=8B=D0=B9_=D1=84?= =?UTF-8?q?=D0=BE=D1=80=D1=83=D0=BC_=C2=AB=D0=9C=D0=A2=D0=9A_=D0=A1=D0=B5?= =?UTF-8?q?=D0=B2=D0=B5=D1=80-=D0=AE=D0=B3_-_=D0=BD=D0=BE=D0=B2=D1=8B?= =?UTF-8?q?=D0=B5_=D0=B3=D0=BE=D1=80=D0=B8=D0=B7=D0=BE=D0=BD=D1=82=D1=8B._?= =?UTF-8?q?=D0=91=D1=8B=D1=81=D1=82=D1=80=D1=8B=D0=B9_=D0=B2=D1=8B=D1=85?= =?UTF-8?q?=D0=BE=D0=B4_=D0=BD=D0=B0_=D0=BF=D1=80=D0=B5=D0=BC=D0=B8=D0=B0?= =?UTF-8?q?=D0=BB=D1=8C=D0=BD=D1=8B=D0=B5_=D1=80=D1=8B=D0=BD=D0=BA=D0=B8_?= =?UTF-8?q?=D0=98=D1=80=D0=B0=D0=BD=D0=B0,_=D0=92=D0=BE=D1=81=D1=82=D0=BE?= =?UTF-8?q?=D0=BA=D0=B0,_=D0=AE=D0=B6=D0=BD=D0=BE=D0=B9_=D0=90=D0=B7=D0=B8?= =?UTF-8?q?=D0=B8,_=D0=92=D0=BE=D1=81=D1=82=D0=BE=D1=87=D0=BD=D0=BE=D0=B9_?= =?UTF-8?q?=D0
 =90=D1=84=D1=80=D0=B8=D0=BA=D0=B8_=D0=B8_=D0=98=D0=BD=D0=B4?= =?UTF-8?q?=D0=B8=D0=B8=C2=BB?=
X-End: headers

this is a body
'''
        def cb(value):
            return value
        s = Suspect("from@fuglu.org", "to@fuglu.org", '/dev/null')
        s.set_source(src)
        subj = s.get_header('subject')
        self.assertIsNotNone(subj)
        subj = s.decode_msg_header(subj)
        self.assertNotIn('=?UTF-8?', subj)
        s.update_subject(cb) # this would raise UnicodeEncodeError if subject is not re-encodeable
        value = fold_header('subject', subj) # this would raise UnicodeEncodeError if subject is not re-encodeable


class TestNoReadDevNull(unittest.TestCase):
    """Don't read from /dev/null"""
    def test_noread(self):
        """Simple check for no exception"""
        s = Suspect("from@fuglu.org", "to@fuglu.org", "/dev/null")
        self.assertEqual(b"", s.get_original_source())

    def test_noread_applytemplate(self):
        """Apply message template will eventually also call get_original_source"""
        s = Suspect("from@fuglu.org", "to@fuglu.org", "/dev/null")
        tmpl = 'result=${result} from_domain=${from_domain} client_address=${client_address} explanation=${explanation}'
        message = apply_template(tmpl, s, dict(result="none", explanation="none", client_address="127.0.0.1"))
        self.assertEqual("result=none from_domain=fuglu.org client_address=127.0.0.1 explanation=none", message)



class TestRemoveHeadersFromSource(unittest.TestCase):
    TESTDATA = \
b"""x-startheader: the first header
x-oneliner: a first header on one line
x-twoliner: a second header spreading across
    two lines
x-threeliner: a third header
    spreading across
    three lines
x-multiheader: a header that exists
x-multiheader: more than once
x-endheader: the last header

start of body
x-notaheader: it contains tricky lines
end of body
"""
    
    HDRCNT = 7
    
    def test_remove_inexixting(self):
        origrep = email.message_from_bytes(self.TESTDATA)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(self.TESTDATA)
        removed = suspect.remove_headers_from_source({'x-doesnotexist'})
        self.assertFalse(removed)
        self.assertEqual(self.TESTDATA, origrep.as_bytes())
    
    def test_remove_oneliner(self):
        origrep = email.message_from_bytes(self.TESTDATA)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(self.TESTDATA)
        removed = suspect.remove_headers_from_source({'x-oneliner'})
        self.assertTrue(removed, 'header did not get removed')
        msgrep = suspect.get_message_rep()
        hdrs = list(msgrep.keys())
        self.assertEqual(len(hdrs), self.HDRCNT-1, f'found {len(hdrs)} headers')
        self.assertNotIn('x-oneliner', hdrs, 'header still found in msgrep')
        self.assertIn('x-oneliner', suspect.removed_headers)
        self.assertEqual(msgrep['x-startheader'], 'the first header')
        self.assertEqual(msgrep['x-endheader'], 'the last header')
        self.assertEqual(len(msgrep.get_payload().splitlines()), 3)
        del origrep['x-oneliner']
        self.assertEqual(msgrep.as_bytes(), origrep.as_bytes())
    
    def test_remove_twoliner(self):
        origrep = email.message_from_bytes(self.TESTDATA)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(self.TESTDATA)
        removed = suspect.remove_headers_from_source({'x-twoliner'})
        self.assertTrue(removed, 'header did not get removed')
        msgrep = suspect.get_message_rep()
        hdrs = list(msgrep.keys())
        self.assertEqual(len(hdrs), self.HDRCNT-1, f'found {len(hdrs)} headers')
        self.assertNotIn('x-twoliner', hdrs, 'header still found in msgrep')
        self.assertIn('x-twoliner', suspect.removed_headers)
        oneliner = msgrep.get('x-oneliner')
        self.assertNotIn('two lines', oneliner)
        self.assertNotIn('three lines', oneliner)
        self.assertEqual(msgrep['x-startheader'], 'the first header')
        self.assertEqual(msgrep['x-endheader'], 'the last header')
        self.assertEqual(len(msgrep.get_payload().splitlines()), 3)
        del origrep['x-twoliner']
        self.assertEqual(msgrep.as_bytes(), origrep.as_bytes())
    
    def test_remove_threeliner(self):
        origrep = email.message_from_bytes(self.TESTDATA)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(self.TESTDATA)
        removed = suspect.remove_headers_from_source({'x-threeliner'})
        self.assertTrue(removed, 'header did not get removed')
        msgrep = suspect.get_message_rep()
        hdrs = list(msgrep.keys())
        self.assertEqual(len(hdrs), self.HDRCNT-1, f'found {len(hdrs)} headers')
        self.assertNotIn('x-threeliner', hdrs, 'header still found in msgrep')
        self.assertIn('x-threeliner', suspect.removed_headers)
        oneliner = msgrep.get('x-oneliner')
        self.assertNotIn('two lines', oneliner)
        self.assertNotIn('three lines', oneliner)
        twoliner = msgrep.get('x-twoliner')
        self.assertNotIn('three lines', twoliner)
        self.assertEqual(msgrep['x-startheader'], 'the first header')
        self.assertEqual(msgrep['x-endheader'], 'the last header')
        self.assertEqual(len(msgrep.get_payload().splitlines()), 3)
        del origrep['x-threeliner']
        self.assertEqual(msgrep.as_bytes(), origrep.as_bytes())
        
    def test_remove_multiheader(self):
        origrep = email.message_from_bytes(self.TESTDATA)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(self.TESTDATA)
        removed = suspect.remove_headers_from_source({'x-multiheader'})
        self.assertTrue(removed, 'header did not get removed')
        msgrep = suspect.get_message_rep()
        hdrs = list(msgrep.keys())
        self.assertEqual(len(hdrs), self.HDRCNT-2, f'found {len(hdrs)} headers')
        self.assertNotIn('x-multiheader', hdrs, 'header still found in msgrep')
        self.assertIn('x-multiheader', suspect.removed_headers)
        self.assertEqual(msgrep['x-startheader'], 'the first header')
        self.assertEqual(msgrep['x-endheader'], 'the last header')
        self.assertEqual(len(msgrep.get_payload().splitlines()), 3)
        del origrep['x-multiheader']
        self.assertEqual(msgrep.as_bytes(), origrep.as_bytes())
    
    def test_do_not_remove_bodyline(self):
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(self.TESTDATA)
        removed = suspect.remove_headers_from_source({'x-notaheader'})
        self.assertFalse(removed, 'something got removed')
        msgrep = suspect.get_message_rep()
        hdrs = list(msgrep.keys())
        self.assertEqual(len(hdrs), self.HDRCNT, f'found {len(hdrs)} headers')
        msgsrc = suspect.get_source()
        self.assertIn(b'x-notaheader', msgsrc, 'sequence not found in source')
        self.assertEqual(len(msgrep.get_payload().splitlines()), 3)
        self.assertEqual(msgsrc, msgrep.as_bytes())
        self.assertEqual(msgsrc, self.TESTDATA)
    
    
    def test_broken_content_umlaut(self):
        TESTDATA = \
b"""x-startheader: the first header
x-oneliner: a first header on one line
Subject: s\xc3\xb6mething with \xc3\xbcmlauts
x-endheader: the last header

just a message body
"""
        origrep = email.message_from_bytes(TESTDATA)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(TESTDATA)
        removed = suspect.remove_headers_from_source({'x-oneliner'})
        self.assertTrue(removed, 'header did not get removed')
        msgrep = suspect.get_message_rep()
        hdrs = list(msgrep.keys())
        self.assertNotIn('x-oneliner', hdrs, 'header still found in msgrep')
        del origrep['x-oneliner']
        self.assertEqual(msgrep.as_bytes(), origrep.as_bytes())
        
    
    def text_broken_content_boundary_space(self):
        TESTDATA = \
            (b"x-startheader: the first header\n"
             b"x-oneliner: a first header on one line\n"
             b"Content-Type: multipart/mixed;\n"
             b"	boundary=\"_boundary_\"\n"
             b"MIME-Version: 1.0\n"
             b"x-endheader: the last header\n"
             b"\n"
             b"--_000_boundary_ \n"
             b"Content-Type: text/plain; charset=\"utf-8\" \n"
             b"\n"
             b"there's a space at the end of the content-type mime header line \n"
             b"--_000_boundary_--\n")
        origrep = email.message_from_bytes(TESTDATA)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(TESTDATA)
        removed = suspect.remove_headers_from_source({'x-oneliner'})
        self.assertTrue(removed, 'header did not get removed')
        msgrep = suspect.get_message_rep()
        hdrs = list(msgrep.keys())
        self.assertNotIn('x-oneliner', hdrs, 'header still found in msgrep')
        del origrep['x-oneliner']
        self.assertEqual(msgrep.as_bytes(), origrep.as_bytes())
        self.assertEqual(suspect.get_source(), origrep.as_bytes())
        suspect.add_header('x-foobar', 'asdfasdf')
        origrep.add_header('x-foobar', 'asdfasdf')
        msgrep = suspect.get_message_rep()
        self.assertEqual(msgrep.as_bytes(), origrep.as_bytes())
        self.assertEqual(suspect.get_source(), origrep.as_bytes())
        
    def _subject_updater(self, subject, new_subject):
        return new_subject
    
    def test_subject_updater(self):
        subjects = [
            'a different subject',
            'a very very very very long subject that should be folded to newline after replacement',
            'ä sübjéct wïth spéçîöl chars',
        ]
        
        TESTDATA = \
b"""x-startheader: the first header
Subject: this is a subject
x-endheader: the last header

just a message body
"""
        
        for newsubject in subjects:
            suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
            suspect.set_source(TESTDATA.replace(b'\n', b'\r\n'))
            suspect.update_subject(self._subject_updater, new_subject=newsubject)
            subject = suspect.get_header('subject')
            msgrep = suspect.get_message_rep()
            print(msgrep['subject'])
            print(msgrep.keys())
            expectation = fold_header('subject', newsubject, value_only=True)
            self.assertEqual(subject, expectation)
            msgrep = suspect.get_message_rep()
            self.assertEqual(msgrep['subject'], expectation)
            source = suspect.get_source()
            self.assertIn(expectation.encode(), source)


    def test_subject_updater_multiline(self):
        """Test subject update doesn't break headers if there's a multiline header to replace"""
        NEWSUBJECT = 'a different subject'
        TESTDATA = \
b"""MIME-Version: 1.0
To: <recipient@fuglu.org>
From: <sender@fuglu.org>
CC: <cc@fuglu.org>
Subject: I am a long
 line
Content-Type: multipart/alternative; 
 boundary="----=_MIME_BOUNDARY_000_12140"

------=_MIME_BOUNDARY_000_12140
Content-Type: text/plain

blablabla

some <tagged>text</tagged>

------=_MIME_BOUNDARY_000_12140
Content-Type: text/html

<h1>blablabla3</h1>

some <tagged>text3</tagged>

------=_MIME_BOUNDARY_000_12140--
""".replace(b"\n", b"\r\n")

        EXPECTED = \
b"""subject: a different subject
MIME-Version: 1.0
To: <recipient@fuglu.org>
From: <sender@fuglu.org>
CC: <cc@fuglu.org>
Content-Type: multipart/alternative; 
 boundary="----=_MIME_BOUNDARY_000_12140"

------=_MIME_BOUNDARY_000_12140
Content-Type: text/plain

blablabla

some <tagged>text</tagged>

------=_MIME_BOUNDARY_000_12140
Content-Type: text/html

<h1>blablabla3</h1>

some <tagged>text3</tagged>

------=_MIME_BOUNDARY_000_12140--
""".replace(b"\n", b"\r\n")

        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(TESTDATA)
        suspect.update_subject(self._subject_updater, new_subject=NEWSUBJECT)

        # check if new subject has been set using the "get_header" method of Suspect
        subject = suspect.get_header('subject')
        self.assertEqual(subject, NEWSUBJECT)

        # check if new subject has been set extracting the python email object and checking manually
        msgrep = suspect.get_message_rep()
        self.assertEqual(msgrep['subject'], NEWSUBJECT)

        # check if new subject has been set looking at the source directly
        source = suspect.get_source()
        self.assertIn(NEWSUBJECT.encode(), source)

        # check source comparing to expected
        self.assertEqual(EXPECTED, source)

        # building message source
        source4reinject = buildmsgsource(suspect)
        self.assertEqual(EXPECTED, source4reinject)

    def test_set_header_multiline(self):
        """Test set header (replace, add) and add header"""
        NEWSUBJECT = 'a different subject'
        TESTDATA = \
b"""MIME-Version: 1.0
To: TO
  <recipient@fuglu.org>
From: SENDER
  <sender@fuglu.org>
CC: CC
  <cc@fuglu.org>
Subject: I am a long
  line
Content-Type: multipart/alternative; 
 boundary="----=_MIME_BOUNDARY_000_12140"

------=_MIME_BOUNDARY_000_12140
Content-Type: text/plain

blablabla

some <tagged>text</tagged>

------=_MIME_BOUNDARY_000_12140
Content-Type: text/html

<h1>blablabla3</h1>

some <tagged>text3</tagged>

------=_MIME_BOUNDARY_000_12140--
""".replace(b"\n", b"\r\n")

        EXPECTED = \
b"""x-test-setunset: test-set-unset
x-test-set: test-set
subject: a different subject
MIME-Version: 1.0
To: TO
  <recipient@fuglu.org>
From: SENDER
  <sender@fuglu.org>
CC: CC
  <cc@fuglu.org>
Content-Type: multipart/alternative; 
 boundary="----=_MIME_BOUNDARY_000_12140"

------=_MIME_BOUNDARY_000_12140
Content-Type: text/plain

blablabla

some <tagged>text</tagged>

------=_MIME_BOUNDARY_000_12140
Content-Type: text/html

<h1>blablabla3</h1>

some <tagged>text3</tagged>

------=_MIME_BOUNDARY_000_12140--
""".replace(b"\n", b"\r\n")

        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(TESTDATA)
        suspect.set_header("subject", NEWSUBJECT)
        suspect.set_header("x-test-set", "test-set")
        suspect.set_header("x-test-setunset", "test-set-unset")
        suspect.add_header("x-test-add", "test-add")

        # check source comparing to expected
        source = suspect.get_source()
        self.assertEqual(EXPECTED, source)

        # building message source, at this point the added header should have been prepended
        source4reinject = buildmsgsource(suspect)
        self.assertEqual(b"x-test-add: test-add\r\n" + EXPECTED, source4reinject)

    def test_add_transport_header(self):
        """
        Test adding a transport header

        Note: A transport has the form

        X-Transport: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa:[example-org.aaaa.aaaaaaaaaa.example.org]:25

        which becomes

        X-Transport: =?utf-8?q?aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa=3A=5Bexample-org?= =?utf-8?q?=2Eaaaa=2Eaaaaaaaaaa=2Eexample=2Eorg=5D=3A25?=

        with default header encoding done by `add_header`. This causes postfix routing after reinject to ignore this header because of the encoding.

        In immediate mode, the option `raw` allows to add a header as-is to prevent encoding.
        """
        TESTDATA = \
b"""MIME-Version: 1.0
To: TO
  <recipient@fuglu.org>
From: SENDER
  <sender@fuglu.org>
CC: CC
  <cc@fuglu.org>
Subject: I am a long
  line
Content-Type: multipart/alternative; 
 boundary="----=_MIME_BOUNDARY_000_12140"

------=_MIME_BOUNDARY_000_12140
Content-Type: text/plain

blablabla

some <tagged>text</tagged>

------=_MIME_BOUNDARY_000_12140
Content-Type: text/html

<h1>blablabla3</h1>

some <tagged>text3</tagged>

------=_MIME_BOUNDARY_000_12140--
""".replace(b"\n", b"\r\n")

        EXPECTED = \
b"""X-Transport: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa:[example-org.aaaa.aaaaaaaaaa.example.org]:25
MIME-Version: 1.0
To: TO
  <recipient@fuglu.org>
From: SENDER
  <sender@fuglu.org>
CC: CC
  <cc@fuglu.org>
Subject: I am a long
  line
Content-Type: multipart/alternative; 
 boundary="----=_MIME_BOUNDARY_000_12140"

------=_MIME_BOUNDARY_000_12140
Content-Type: text/plain

blablabla

some <tagged>text</tagged>

------=_MIME_BOUNDARY_000_12140
Content-Type: text/html

<h1>blablabla3</h1>

some <tagged>text3</tagged>

------=_MIME_BOUNDARY_000_12140--
""".replace(b"\n", b"\r\n")

        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_source(TESTDATA)
        suspect.add_header("X-Transport", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa:[example-org.aaaa.aaaaaaaaaa.example.org]:25", immediate=True, raw=True)

        # building message source, at this point the added header should have been prepended
        source4reinject = buildmsgsource(suspect)
        self.assertEqual(EXPECTED, source4reinject)
        #self.assertEqual(b"x-test-add: test-add\r\n" + EXPECTED, source4reinject)


class TestHeaderManipulation(unittest.TestCase):
    """Tests keeping track of header changes to be applied later in milter mode"""
    def test_set_header(self):
        """Set header test if header is already present"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msgrep = suspect.get_message_rep()
        self.assertIn("From", msgrep.keys())
        suspect.set_header("From", "Sender <sender@unittest.fuglu.org>")

        self.assertEqual({"From": "Sender <sender@unittest.fuglu.org>"}, suspect.modified_headers)
        self.assertEqual({}, suspect.added_headers, "added_headers keeps tracks of headers already added -> should be empty")
        self.assertEqual({}, suspect.addheaders, "added_headers keeps tracks of headers to be added -> should be empty")
        self.assertEqual({}, suspect.removed_headers, "removed_headers keeps tracks of headers to be removed -> should be empty")

    def test_set_header_by_add(self):
        """Set header test if header is not yet present"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msgrep = suspect.get_message_rep()
        self.assertNotIn("sender", msgrep.keys())
        suspect.set_header("sender", "Sender <sender@unittest.fuglu.org>")

        self.assertEqual({"sender": "Sender <sender@unittest.fuglu.org>"}, suspect.added_headers)
        self.assertEqual({}, suspect.modified_headers, "modified_headers keeps tracks of headers already added -> should be empty")
        self.assertEqual({}, suspect.addheaders, "added_headers keeps tracks of headers to be added -> should be empty")
        self.assertEqual({}, suspect.removed_headers, "removed_headers keeps tracks of headers to be removed -> should be empty")

    def test_update_subject(self):
        """Update subject header test"""
        suspect = Suspect('sender@unittests.fuglu.org',
                          'recipient@unittests.fuglu.org', TESTDATADIR + '/helloworld.eml')
        msgrep = suspect.get_message_rep()
        self.assertIn("Subject", msgrep.keys())

        def dummyupdate(oldsubject: str, **kwars):
            return "this is the new updated subject"

        suspect.update_subject(dummyupdate)

        msgrep = suspect.get_message_rep()
        self.assertEqual("this is the new updated subject", msgrep["Subject"])

        self.assertEqual({"subject": "this is the new updated subject"}, suspect.modified_headers)
        self.assertEqual({}, suspect.added_headers, "added_headers keeps tracks of headers already added -> should be empty")
        self.assertEqual({}, suspect.addheaders, "added_headers keeps tracks of headers to be added -> should be empty")
        self.assertEqual({}, suspect.removed_headers, "removed_headers keeps tracks of headers to be removed -> should be empty")


class TestGlobalMethods(unittest.TestCase):
    """Tests for global methods in shared module"""
    
    def test_redact_uri(self):
        """Testing helper routine redacting password in uris"""
        tests = {
            'https://example.com': 'https://example.com',
            'https://example.com/': 'https://example.com/',
            'https://example.com/foo': 'https://example.com/foo',
            'https://example.com:8443': 'https://example.com:8443',
            'https://example.com:8443/': 'https://example.com:8443/',
            'https://user@example.com/': 'https://user@example.com/',
            'https://:pass@example.com/': 'https://:*****@example.com/',
            'https://user:pass@example.com/': 'https://user:*****@example.com/',
            'https://user:pass@example.com/some/path': 'https://user:*****@example.com/some/path',
        }
        for test in tests:
            result = redact_uri(test)
            self.assertEqual(tests[test], result, f'invalid redacted uri, expected {tests[test]} got {result}')
        
        
        