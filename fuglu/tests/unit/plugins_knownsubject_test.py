# -*- coding: UTF-8 -*-
import unittest
import logging
import sys
import os
from fuglu.plugins.knownsubject import KnownSubject
from fuglu.shared import Suspect, FuConfigParser
from .unittestsetup import TESTDATADIR


def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

class TestSpamSubjectMixing(unittest.TestCase):
    def test_specialchars_subject(self,):
        """Test mail subject with "ä" but not encoded"""

        filename = os.path.join(TESTDATADIR, "subject_umlaut.eml")
        config = FuConfigParser()
        config.add_section('test')
        ssubj = KnownSubject(config, 'test')
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
        msg = suspect.get_message_rep()
        origsubject = suspect.get_header('subject')
        subject = suspect.decode_msg_header(origsubject)
        subject = ssubj._normalise_subject(subject, msg['To'])
        self.assertEqual("diezeitlUUuftab", subject, f"original subject from mail {origsubject} hash {subject} expect diezeitlUUuftab")

    def test_specialchars_subject2(self,):
        """Test mail subject with special char but no encoding"""

        try:
            from domainmagic.util import unconfuse
            DOMAINMAGIC_AVAILABLE = True
        except ImportError:
            DOMAINMAGIC_AVAILABLE = False

        self.assertTrue(DOMAINMAGIC_AVAILABLE, f"No domainmagic available!")

        filename = os.path.join(TESTDATADIR, "subject_nonunichar.eml")
        config = FuConfigParser()
        config.add_section('test')
        ssubj = KnownSubject(config, 'test')

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
        msg = suspect.get_message_rep()
        origsubject = suspect.get_header('subject')
        subject = suspect.decode_msg_header(origsubject)
        subject = ssubj._normalise_subject(subject, msg['To'])
        self.assertEqual("klassedasfunktioniertUUobsiemitm", subject, f"original subject from mail {origsubject} hash {subject} expect klassedasfunktioniertUUobsiemitm")
    
    def test_lint(self):
        config = FuConfigParser()
        config.add_section('test')
        ssubj = KnownSubject(config, 'test')
        ssubj.lint()