# -*- coding: UTF-8 -*-
import unittest
from .unittestsetup import TESTDATADIR
from fuglu.extensions.fuzzyhashlib import hashers_available, digest

class TestFuzzyHash(unittest.TestCase):
    def test_ssdeep(self):
        if not 'ssdeep' in hashers_available:
            print('ssdeep not available')
            return
        with open(TESTDATADIR + '/dummy.doc.zip', 'rb') as f:
            data = f.read()
        fh = digest('ssdeep', data)
        self.assertEqual('6:5hklw+r01AkEDAX5ymzj+PEj83wUZwUNmEpjt+l27:5hsw+w1A1kX51P+8jUwswepBaq', fh, 'wrong ssdeep hash')

    
    def test_imagehash(self):
        if not 'imagehash_average' in hashers_available:
            print('imagehash not available')
            return
        with open(TESTDATADIR + '/test.png', 'rb') as f:
            data = f.read()
        
        values = {
            'imagehash_average': 'ffc703290b0be7ff',
            'imagehash_color': '07000000000',
            'imagehash_cropresistant': '04af4f525b160c00',
            'imagehash_dhash': '04af4f525b160c00',
            'imagehash_phash': 'b306c4f9c9c61d33',
            'imagehash_phashsimple': '00004400ccccc972',
            'imagehash_whash': 'ff4301080b03e7ff',
        }
        
        for hsr, exp in values.items():
            fh = digest(hsr, data)
            self.assertEqual(exp, fh, f'wrong {hsr} hash')
    
    def test_opencvhash(self):
        if not 'opencvhash_average' in hashers_available:
            print('opencv/cv2 or numpy not available')
            return
        with open(TESTDATADIR + '/test.png', 'rb') as f:
            data = f.read()
        
        values = {
            'opencvhash_average': 'ffeff7fdf3fbffff',
            'opencvhash_blockmean': 'fffffffffffc4df025ec61d831b3a5e725e315f389f39bf13ff87ffeffffffff',
            #'opencvhash_colormoment': '', # hash is excessively long (700 chars)
            # 'opencvhash_marhildreth': '', # hash is long (100 chars) and not stable for unchanged image
            'opencvhash_phash': '9c60631ec3c398d8',
            'opencvhash_radialvariance': '8fd0951c64e9bfd297004691a0ff9e6c4765dad8976f5a76aa93819a7c83858cb6998c856892b292',
        }
        
        for hsr, exp in values.items():
            fh = digest(hsr, data)
            self.assertEqual(exp, fh, f'wrong {hsr} hash')