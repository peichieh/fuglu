# -*- coding: UTF-8 -*-
import os
import unittest
import secrets
from .unittestsetup import TESTDATADIR
from fuglu.plugins.p_blwl import BlockWelcomeEntry, FugluBlockWelcome, AutoListAppender, AutoListBackend, STAGE_PREPENDER, JsonFile
from fuglu.shared import Suspect, FuConfigParser, DUNNO
from fuglu.asyncprocpool import get_event_loop


class FugluBlockWelcomeTest(FugluBlockWelcome):
    def __init__(self, config, section, listings):
        super().__init__(config, section)
        self.listing_data = listings
    
    def _get_listings(self, suspectid=''):
        listings = {}
        for item in self.listing_data:
            listing = BlockWelcomeEntry(**item)
            scope = item['scope']
            try:
                listings[scope].append(listing)
            except KeyError:
                listings[scope] = [listing]
        return listings


class FugluBlockWelcomeTestCase(unittest.IsolatedAsyncioTestCase):
    
    def setUp(self):
        config = FuConfigParser()
        section = 'BlockWelcome'
        config.add_section(section)
        config.set(section, 'fublwl_eval_order', 'user,domain,global')
        config.set(section, 'fublwl_update_hitcount', 'False')
        config.set(section, 'fublwl_header_checks', 'False')
        config.set(section, 'fublwl_header_host_only', 'False')
        config.set(section, 'fublwl_debug', 'True')
        config.set(section, 'fublwl_restapi_endpoint', '')
        config.set(section, 'fublwl_restapi_key_map', '')
        config.set(section, 'fublwl_json_file', '/dev/zero')
        self.config = config
        self.section = section
    
    def tearDown(self):
        pass
    
    async def test_listing_ip(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': 32, 'scope': '$GLOBAL', 'sender_addr': None, 'sender_host': '8.8.8.8'},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'ip {suspect.clientinfo[1]} not welcomelisted')
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '9.9.9.9', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), f'ip {suspect.clientinfo[1]} is welcomelisted')
        
        
    async def test_listing_sender_email(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': -1, 'scope': '$GLOBAL', 'sender_addr': 'sender@unittests.fuglu.org', 'sender_host': None},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'sender {suspect.from_address} not welcomelisted')
        
        suspect = Suspect('somebody@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), f'sender {suspect.from_address} is welcomelisted')
    
    
    async def test_listing_sender_dom(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': -1, 'scope': '$GLOBAL', 'sender_addr': '%unittests.fuglu.org', 'sender_host': None},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'sender {suspect.from_domain} not welcomelisted')
        
        suspect = Suspect('somebody@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'sender {suspect.from_domain} not welcomelisted')
        
        suspect = Suspect('sender@othertests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), f'sender {suspect.from_domain} is welcomelisted')
    
    
    def test_listing_active(self):
        config = self.config
        config.set(self.section, 'fublwl_json_file', os.path.join(TESTDATADIR, 'listings.json'))
        config.set(self.section, 'fublwl_restapi_key_map', "listID:list_id, listType:list_type, listScope:list_scope, senderAddress:sender_addr, senderHost:sender_host")
        candidate = FugluBlockWelcome(config, self.section)
        used_listing = candidate._get_listings()
        print(used_listing)
        self.assertIn('$GLOBAL', used_listing)
        self.assertEqual(len(used_listing['$GLOBAL']), 1)
        self.assertEqual(used_listing['$GLOBAL'][0].sender_addr, 'active@unittests.fuglu.org')


class AutoListAppenderTest(AutoListAppender):
    async def _store_redis(self, suspect, rediskey, increment):
        self.rediskey = rediskey
        self.increment = increment

class TestAutoListAppender(unittest.TestCase):
    def setUp(self):
        config = FuConfigParser()
        section = 'AutoListAppender'
        config.add_section(section)
        config.set(section, 'increment_overrides', os.path.join(TESTDATADIR, "autolist_overrides.txt"))
        config.set(section, 'al_redis_conn', 'redis://redis:6379/1')
        config.set(section, 'al_redis_timeout', '3')
        config.set(section, 'al_redis_ttl', '10')
        config.set(section, 'al_max_count', '10')
        self.config = config
        self.section = section
        
    def test_gen_key(self):
        candidate = AutoListAppender(self.config, self.section)
        key = candidate._gen_key('x', 'y')
        self.assertEqual(key, 'y\nx')
    
    def test_header_skip(self):
        config = FuConfigParser()
        section = 'AutoListAppender'
        config.add_section(section)
        config.set(section, 'al_skip_headers', 'X-Example')
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        candidate = AutoListAppender(config, section)
        skip = candidate._header_skip(suspect)
        self.assertFalse(skip)
        suspect.set_header('X-Example', 'foobar')
        skip = candidate._header_skip(suspect)
        self.assertTrue(skip)
        
    def test_envelope_skip(self):
        candidate = AutoListAppender(self.config, self.section)
        skip = candidate._envelope_skip('x@example.com', 'y@example.com')
        self.assertFalse(skip)
        skip = candidate._envelope_skip('x@example.com', 'x@example.com')
        self.assertTrue(skip)
        skip = candidate._envelope_skip('<>', 'x@example.com')
        self.assertTrue(skip)
        skip = candidate._envelope_skip('', 'x@example.com')
        self.assertTrue(skip)
        skip = candidate._envelope_skip('x', 'y@example.com')
        self.assertTrue(skip)
        skip = candidate._envelope_skip('x', 'y')
        self.assertTrue(skip)
        
    def test_envelope_override(self):
        config = FuConfigParser()
        section = 'AutoListAppender'
        config.add_section(section)
        config.set(section, 'al_header_as_env_sender', 'X-Example')
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        candidate = AutoListAppender(config, section)
        env_headers = config.getlist(self.section, 'al_header_as_env_sender')
        val = candidate._get_env_override(suspect, env_headers)
        self.assertIsNone(val)
        suspect.set_header('X-Example', 'foo@bar.com')
        val = candidate._get_env_override(suspect, env_headers)
        self.assertEqual(val, 'foo@bar.com')
    
    def test_overrides(self):
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        candidate = AutoListAppender(self.config, self.section)
        increment = candidate._get_increment(suspect, 'sender@unittests.fuglu.org', False)
        self.assertEqual(increment, 1)
        increment = candidate._get_increment(suspect, 'sender@unittests.fuglu.org', True)
        self.assertEqual(increment, 10)
        increment = candidate._get_increment(suspect, 'sender@example.fuglu.org', True)
        self.assertEqual(increment, 15)
        increment = candidate._get_increment(suspect, 'special@example.fuglu.org', True)
        self.assertEqual(increment, 20)
    
    def test_full(self):
        suspect = Suspect('sender@example.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        candidate = AutoListAppenderTest(self.config, self.section)
        candidate.process(suspect, DUNNO)
        self.assertEqual(candidate.increment, 1)
        self.assertEqual(candidate.rediskey, 'recipient@unittests.fuglu.org\nsender@example.fuglu.org')
    
    def test_set_get(self):
        key = secrets.token_hex(32)
        suspect = Suspect('sender@example.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        appender = AutoListAppender(self.config, self.section)
        backend = AutoListBackend(self.config, self.section)
        event_loop = get_event_loop()
        event_loop.run_until_complete(backend._reset(key)) # initial cleanup
        val = event_loop.run_until_complete(backend._get_count(suspect, key))
        self.assertEqual(val, 0, f'expected count=0, got {val}')
        event_loop.run_until_complete(appender._store_redis(suspect, key, 1))
        val = event_loop.run_until_complete(backend._get_count(suspect, key))
        self.assertEqual(val, 1, f'expected count=1 after store, got {val}')
        val = event_loop.run_until_complete(backend._reset(key))
        self.assertEqual(val, 1, f'failed to delete key {key}')
        val = event_loop.run_until_complete(backend._get_count(suspect, key))
        self.assertEqual(val, 0, f'expected count=0 after reset, got {val}')
    
    def test_cleanup(self):
        """
        Test cleanup in aioredisext ExpiringCounter
        """
        key = secrets.token_hex(32)
        backend = AutoListBackend(self.config, self.section)
        event_loop = get_event_loop()
        event_loop.run_until_complete(backend._reset(key)) # initial cleanup
        val = event_loop.run_until_complete(backend.backend_redis.get_count(key))
        self.assertEqual(val, 0, f'expected count=0, got {val}')
        backend.backend_redis.ttl = 0 # the next entry should expire immediately
        event_loop.run_until_complete(backend.backend_redis.increase(key))
        backend.backend_redis.ttl = -1 # the next entry should be outdate but not expire
        event_loop.run_until_complete(backend.backend_redis.increase(key))
        backend.backend_redis.ttl = 60 # the next entry should remain during test run
        event_loop.run_until_complete(backend.backend_redis.increase(key))
        event_loop.run_until_complete(backend.backend_redis.cleanup())
        val = event_loop.run_until_complete(backend.backend_redis.get_count(key))
        self.assertEqual(val, 1, f'expected count=1 after cleanup, got {val}')
        val = event_loop.run_until_complete(backend._reset(key))
        self.assertEqual(val, 1, f'failed to delete key {key}')
        val = event_loop.run_until_complete(backend.backend_redis.get_count(key))
        self.assertEqual(val, 0, f'expected count=0 after reset, got {val}')
        

class JsonFileLoadTestCase(unittest.TestCase):
    def test_load_file(self):
        key_map = [x.strip() for x in "listID:list_id, listType:list_type, listScope:list_scope, senderAddress:sender_addr, senderHost:sender_host, netmask:netmask, scope:scope, hitcount:hitcount".split(',')]
        jsonfile = JsonFile(filename=os.path.join(TESTDATADIR, 'blwl.json'), key_map=key_map)
        listings = jsonfile.get_list()
        self.assertIn('%unittests.fuglu.org', listings)
        self.assertEqual(len(listings['%unittests.fuglu.org']), 2)
        self.assertEqual(listings['%unittests.fuglu.org'][0].list_id, 3)
        self.assertEqual(listings['%unittests.fuglu.org'][1].list_id, 1)
        
        self.assertIn('rcpt@unittests.fuglu.org', listings)
        self.assertEqual(len(listings['rcpt@unittests.fuglu.org']), 4)
        self.assertEqual(listings['rcpt@unittests.fuglu.org'][0].list_id, 4)
        self.assertEqual(listings['rcpt@unittests.fuglu.org'][1].list_id, 5)
        self.assertEqual(listings['rcpt@unittests.fuglu.org'][2].list_id, 2)
        self.assertEqual(listings['rcpt@unittests.fuglu.org'][3].list_id, 6)


class JsonFileBLWLTestCase(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        config = FuConfigParser()
        section = 'BlockWelcome'
        config.add_section(section)
        config.set(section, 'fublwl_eval_order', 'user,domain,global')
        config.set(section, 'fublwl_update_hitcount', 'False')
        config.set(section, 'fublwl_header_checks', 'False')
        config.set(section, 'fublwl_header_host_only', 'False')
        config.set(section, 'fublwl_debug', 'True')
        config.set(section, 'fublwl_restapi_endpoint', '')
        config.set(section, 'fublwl_restapi_key_map', "listID:list_id, listType:list_type, listScope:list_scope, senderAddress:sender_addr, senderHost:sender_host, netmask:netmask, scope:scope, hitcount:hitcount")
        config.set(section, 'fublwl_json_file', os.path.join(TESTDATADIR, 'blwl.json'))
        self.config = config
        self.section = section
        
    async def test_listing(self):
        candidate = FugluBlockWelcome(self.config, self.section)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '::1', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), '::1 not listed')
        self.assertTrue(suspect.tags.get('welcomelisted.confirmed'), '::1 unconfirmed listing')
        
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '::2', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), '::2 listed')
        self.assertFalse(suspect.tags.get('welcomelisted.confirmed'), '::2 confirmed listing')
        
        suspect = Suspect('sender@example.com', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '::1', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), '::1 not listed')
        self.assertFalse(suspect.tags.get('welcomelisted.confirmed'), '::1 confirmed listing')
        
        suspect = Suspect('sender@example.com', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '::2', 'mail.example.com'
        await candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), '::2 listed')
        self.assertFalse(suspect.tags.get('welcomelisted.confirmed'), '::2 confirmed listing')
    
    