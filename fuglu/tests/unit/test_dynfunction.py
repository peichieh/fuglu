# -*- coding: UTF-8 -*-
import unittest
import ipaddress
from fuglu.plugins.ratelimit.dynfunction import FunctionWrapper
from unittest.mock import patch, MagicMock


class TestDynfunction(unittest.TestCase):
    def test_domainextract(self):
        """Test domain extraction"""
        definitionstring = "fuglu.plugins.ratelimit.helperfuncs.get_domain_from_uri"
        f = FunctionWrapper(definitionstring=definitionstring)
        self.assertEqual("fuglu.org", f("https://palim.fuglu.org"))
        self.assertEqual("fuglu.org", f("palim.fuglu.org"))

    def test_helo2hostname_domain(self):
        """Test helo split to hostname & domain"""
        defhost = "fuglu.plugins.ratelimit.helperfuncs.split_helo2host_domain(suspect=)[0]"
        fhost = FunctionWrapper(definitionstring=defhost)
        defdomain = "fuglu.plugins.ratelimit.helperfuncs.split_helo2host_domain(suspect=)[1]"
        fdomain = FunctionWrapper(definitionstring=defdomain)

        self.assertEqual("fuglu.org", fdomain("mailserver.fuglu.org"))
        self.assertEqual("mailserver", fhost("mailserver.fuglu.org"))


class TestHelper(unittest.TestCase):

    def _get_reader_mock(self):
        # mock answer -> Reader -> asn -> response object
        readermock = MagicMock()
        returnmock = MagicMock()
        returnmock.autonomous_system_number = -1
        returnmock.autonomous_system_organization = "Dummy"
        returnmock.ip_address = "192.168.1.100"
        returnmock.network = ipaddress.IPv4Network("192.168.1.0/24")

        readermock.asn.return_value = returnmock
        return readermock

    @patch('geoip2.database.Reader')
    def test_asn(self, geoipreader):
        """Test asn extractor"""
        from fuglu.plugins.ratelimit.helperfuncs import asn

        geoipreader.return_value = self._get_reader_mock()

        asnout = asn('192.168.1.100', geoipfilename='/path/to/datafile/filename.extension')
        print(f"asn output: {asnout}")
        print(f"asn output: {[type(a) for a in asnout]}")

        self.assertEqual(asnout, (-1,
                                  "Dummy",
                                  "192.168.1.100",
                                  ipaddress.IPv4Network("192.168.1.0/24")
                                  )
                         )
