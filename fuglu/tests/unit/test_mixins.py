# -*- coding: UTF-8 -*-
import time
import unittest
from fuglu.mixins import (
    DefConfigMixin,
    SimpleTimeoutMixin
)
from configparser import NoOptionError

from fuglu.shared import ScannerPlugin, FuConfigParser



class TestDefConfigMixin(unittest.TestCase):
    """Test FuConfig using mixin"""

    def test_configwrap(self):
        """Test if mixin does setup defaults from requiredvars"""
        class _Example(DefConfigMixin):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.requiredvars = {
                    'var1': {
                        'default': 'default string',
                        'description': 'description for var1',
                    }
                }
        configtext = """
        [test]
        var1 = bla
        """
        config = FuConfigParser()
        config.read_string(configtext)

        ex = _Example(config=config)

        self.assertTrue(config._defaultdict, "Setting up Plugin should setup defaultdict in FuConfigParser")
        self.assertTrue(ex.config._defaultdict, "Setting up Plugin should setup defaultdict in plugins config")

    def test_defaultdictchange(self):
        """Test default change"""
        class _Example(DefConfigMixin):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.requiredvars = {
                    'var1': {
                        'default': 'default string',
                        'description': 'description for var1',
                    }
                }
        configtext = """
        [test]
        """
        config = FuConfigParser()
        config.read_string(configtext)

        ex = _Example(config=config, section="test")
        var1 = ex.config.get("test", "var1")
        print(f"After initial -> var1: {var1}")
        self.assertEqual("default string", var1)

        ex.requiredvars = {
            'var1': {
                'default': 'new default',
                'description': 'description for var1',
            }
        }

        var1 = ex.config.get("test", "var1")
        print(f"After new dict -> var1: {var1}")
        self.assertEqual("new default", var1)

    def test_defaultreturn(self):
        """base tests for var with defaults"""

        class _Example(DefConfigMixin):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.requiredvars = {
                    'var1': {
                        'default': 'default string',
                        'description': 'description for var1',
                    }
                }

        configtext = """
        [test]
        var1 = bla
        """
        config = FuConfigParser()
        config.read_string(configtext)

        ex = _Example(config=config)
        var1 = ex.config.get("test", "var1")
        self.assertEqual("bla", var1)

        with self.assertRaises(NoOptionError):
            _ = ex.config.getboolean("test", "var2")


class TestPluginMixin(unittest.TestCase):
    """Test if mixin works applied to a real plugin"""

    class _Plugin(ScannerPlugin):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.requiredvars = {
                'var1': {
                    'default': 'default string',
                    'description': 'description for var1',
                },
                'var2': {
                    'default': 'default string',
                    'description': 'description for var1',
                }
            }

    def test_plugin_set(self):
        """Test if parser is wrapped due to mixin"""

        configtext = """
        [test]
        var1 = bla
        """
        config = FuConfigParser()
        config.read_string(configtext)

        pl = TestPluginMixin._Plugin(config=config, section="test")

        var1 = pl.config.get("test", "var1")
        print(f"Scanner plugin -> var1: {var1}")
        self.assertEqual("bla", var1)

    def test_plugin_default(self):
        """Test if parser is wrapped due to mixin"""

        configtext = """
        [test]
        var1 = bla
        """
        config = FuConfigParser()
        config.read_string(configtext)

        pl = TestPluginMixin._Plugin(config=config, section="test")

        var2 = pl.config.get("test", "var2")
        print(f"Scanner plugin -> var2: {var2}")
        self.assertEqual(var2, "default string")


class TestSimpleTimeout(unittest.TestCase):
    """Test timeout mixing directly as standalone class"""
    def test_timout(self):
        simpletout = SimpleTimeoutMixin()
        self.assertTrue(simpletout.stimeout_continue("undefined"))
        simpletout.stimeout_set_timer("test: t=0.2", 0.2)
        simpletout.stimeout_set_timer("test: t=20.0", 20.0)
        self.assertTrue(simpletout.stimeout_continue("test: t=0.2"))
        time.sleep(0.25)
        self.assertFalse(simpletout.stimeout_continue("test: t=0.2"))
        self.assertTrue(simpletout.stimeout_continue("test: t=20.0"))
