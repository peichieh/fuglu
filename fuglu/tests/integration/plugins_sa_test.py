# -*- coding: UTF-8 -*-
#import integrationtestsetup
import unittest
from fuglu.plugins.sa import SAPlugin
import os
from fuglu.shared import DUNNO, REJECT, DEFER, Suspect, FuConfigParser
import fuglu.extensions.sql

from fuglu.extensions.sql import get_session, DBConfig, SQL_EXTENSION_ENABLED, text, sql_alchemy_version, SQL_ALCHEMY_V1, SQL_ALCHEMY_V2

class SAPluginTestCase(unittest.TestCase):

    """Testcases for the Stub Plugin"""

    def setUp(self):
        if not fuglu.extensions.sql.ENABLED:
            """Only works if sql enabled due to blocklist extension"""
            return

        config = FuConfigParser()
        config.add_section('main')
        config.set('main', 'prependaddedheaders', 'X-Fuglu-')

        config.add_section('SAPlugin')
        config.set('SAPlugin', 'host', 'spamd')
        config.set('SAPlugin', 'port', '783')
        config.set('SAPlugin', 'timeout', '5')
        config.set('SAPlugin', 'retries', '3')
        config.set('SAPlugin', 'peruserconfig', '0')
        config.set('SAPlugin', 'maxsize', '500000')
        config.set('SAPlugin', 'spamheader', 'X-Spam-Status')
        config.set('SAPlugin', 'lowspamaction', 'DUNNO')
        config.set('SAPlugin', 'highspamaction', 'REJECT')
        config.set('SAPlugin', 'problemaction', 'DEFER')
        config.set('SAPlugin', 'highspamlevel', '15')
        config.set('SAPlugin', 'forwardoriginal', 'False')
        config.set('SAPlugin', 'scanoriginal', 'False')
        config.set('SAPlugin', 'rejectmessage', '')
        config.set('SAPlugin', 'strip_oversize', '0')

        # sql blacklist
        self.testfile = f"/tmp/sa_test-{Suspect.generate_id()}.db"  # create unique file so parallel tests don't interfere
        # important: 4 slashes for absolute paths!
        self.testdb = "sqlite:///%s" % self.testfile


        session = fuglu.extensions.sql.get_session(self.testdb)
        
        createsql = """CREATE TABLE userpref (
prefid INTEGER NOT NULL PRIMARY KEY,
username varchar(100) NOT NULL DEFAULT '',
preference varchar(30) NOT NULL DEFAULT '',
value varchar(100) NOT NULL DEFAULT ''
)"""
        if sql_alchemy_version == SQL_ALCHEMY_V1:
            session.execute(createsql, {})
        elif sql_alchemy_version == SQL_ALCHEMY_V2:
            try:
                session.execute(text(createsql), {})
                session.commit()
            except Exception:
                print(traceback.format_exc())
                session.rollback()
            finally:
                session.close()
        config.set('SAPlugin', 'sql_blocklist_dbconnectstring', self.testdb)

        self.candidate = SAPlugin(config)

    def tearDown(self):
        # remove db file
        if not fuglu.extensions.sql.ENABLED:
            """Only works if sql enabled due to blocklist extension"""
            return

        try:
            if os.path.exists(self.testfile):
                os.remove(self.testfile)
        except Exception:
            pass

    def test_lint(self):
        """Basic lint test - splitting internal lint subroutines"""
        if not fuglu.extensions.sql.ENABLED:
            """Only works if sql enabled due to blocklist extension"""
            return

        check_config = self.candidate.check_config() 
        print(f"Lint check: config = {check_config}")
        check_ping = self.candidate._lint_ping() 
        print(f"Lint check: ping = {check_ping}")
        check_spam = self.candidate._lint_spam() 
        print(f"Lint check: spam= {check_spam}")
        check_blocklist = self.candidate._lint_blocklist()
        print(f"Lint check: blocklist = {check_blocklist}")

        self.assertTrue(all((check_config, check_ping, check_spam, check_blocklist)))

    def test_score(self):
        if not fuglu.extensions.sql.ENABLED:
            """Only works if sql enabled due to blocklist extension"""
            return

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        stream = """Date: Mon, 08 Sep 2008 17:33:54 +0200
To: oli@unittests.fuglu.org
From: oli@unittests.fuglu.org
Subject: test scanner

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X
"""
        suspect.set_source(stream)
        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        try:
            score = int(suspect.get_tag('SAPlugin.spamscore'))
        except TypeError:
            self.assertTrue(False, f"GTUBE scan result should be a number, got {suspect.get_tag('SAPlugin.spamscore')} - was spamd unavailable?")
            score = 0
        self.assertTrue(score > 999, "GTUBE mails should score ~1000 , we got %s" % score)
        self.assertTrue(result == REJECT, 'High spam should be rejected')

    def test_symbols(self):
        if not fuglu.extensions.sql.ENABLED:
            """Only works if sql enabled due to blocklist extension"""
            return

        stream = """Date: Mon, 08 Sep 2008 17:33:54 +0200
To: oli@unittests.fuglu.org
From: oli@unittests.fuglu.org
Subject: test scanner

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X
"""
        retval = self.candidate.safilter_symbols(stream, 'oli@unittests.fuglu.org')
        self.assertIsNotNone(retval, "no symbols found in SYMBOL scan - was spamd unavailable?")
        
        spamstatus, spamscore, rules = retval
        self.assertTrue('GTUBE' in rules, "GTUBE not found in SYMBOL scan")
        self.assertFalse(spamscore < 500)
        self.assertTrue(spamstatus)

        stream2 = """Received: from mail.python.org (mail.python.org [82.94.164.166])
    by bla.fuglu.org (Postfix) with ESMTPS id 395743E03A5
    for <recipient@unittests.fuglu.org>; Sun, 22 Aug 2010 18:15:11 +0200 (CEST)
Date: Tue, 24 Aug 2010 09:20:57 +0200
To: oli@unittests.fuglu.org
From: oli@unittests.fuglu.org
Subject: test Tue, 24 Aug 2010 09:20:57 +0200
X-Mailer: swaks v20061116.0 jetmore.org/john/code/#swaks
Message-Id: <20100824072058.282BC3E0154@fumail.leetdreams.ch>

This is a test mailing """
        spamstatus, spamscore, rules = self.candidate.safilter_symbols(stream2, 'oli@unittests.fuglu.org')
        self.assertFalse(spamstatus, "This message should not be detected as spam")

    def test_sql_blocklist(self):
        if not fuglu.extensions.sql.ENABLED:
            """Only works if sql enabled due to blocklist extension"""
            return

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        
        session = fuglu.extensions.sql.get_session(self.testdb)
        
        action, message = self.candidate.check_sql_blocklist(suspect)
        self.assertEqual(action, DUNNO), 'sender is not blacklisted'
        
        insertsql = """INSERT INTO userpref (prefid,username,preference,value) VALUES (1,'%unittests.fuglu.org','blacklist_from','*@unittests.fuglu.org')"""
        if sql_alchemy_version == SQL_ALCHEMY_V1:
            session.execute(insertsql, {})
        elif sql_alchemy_version == SQL_ALCHEMY_V2:
            try:
                session.execute(text(insertsql), {})
                session.commit()
            except Exception:
                print(traceback.format_exc())
                session.rollback()
            finally:
                session.close()
        
        action, message = self.candidate.check_sql_blocklist(suspect)
        self.assertEqual(action, REJECT), 'sender should be blacklisted'
        
        # this just doesn't work:
        #fuglu.extensions.sql.SQL_EXTENSION_ENABLED = False  # disable sql (backup is in ENABLED)
        #action, message = self.candidate.check_sql_blocklist(suspect)
        #self.assertEqual(action, DUNNO), 'problem if sqlalchemy is not available'
        #fuglu.extensions.sql.SQL_EXTENSION_ENABLED = fuglu.extensions.sql.ENABLED   # set value back
        
        # simulate unavailable db
        self.candidate.config.set('SAPlugin', 'sql_blocklist_dbconnectstring', 'mysql://127.0.0.1:9977/idonotexist')
        action, message = self.candidate.check_sql_blocklist(suspect)
        self.assertEqual(action, DEFER), 'error coping with db problems'
