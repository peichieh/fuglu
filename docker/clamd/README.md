# build
call build.sh or issue manual command such as following
```
$ docker build -t clamd_img -f Dockerfile  ../../
```

# check built image
```
$ docker images | grep clamd
clamd_img                                       latest        9092f3790898   4 minutes ago   55.7MB
```

# start clamd as container
```
$ docker run -p3310:3310 -d  --name clamd clamd_img
$ docker ps | grep clamd
0151913ea311   clamd_img                                                   "/init clamd --foreg…"   11 seconds ago      Up 10 seconds (health: starting)   3310/tcp, 7357/tcp                                                                 clamd
```

# check everything okay
```
$ docker logs clamd
LibClamAV debug: Initialized 1.2.1 engine
LibClamAV debug: Initializing phishcheck module
LibClamAV debug: Phishcheck: Compiling regex: ^ *(http|https|ftp:(//)?)?[0-9]{1,3}(\.[0-9]{1,3}){3}[/?:]? *$
LibClamAV debug: Phishcheck module initialized
LibClamAV debug: Bytecode initialized in interpreter mode
LibClamAV debug: clean_cache_init: Requested cache size: 65536. Actual cache size: 65536. Trees: 256. Nodes per tree: 256.
LibClamAV debug: Loading databases from /var/lib/clamav
...
...
```
